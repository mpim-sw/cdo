\section{Installation}
{\CDO} is supported in different operative systems such as Unix, macOS and Windows. This section describes how to install {\CDO} in those platforms. More examples are found on the main website (\url{https://code.mpimet.mpg.de/projects/cdo/wiki})
\subsection{Unix}\label{sec:unix}
\subsubsection{Prebuilt CDO packages}
Prebuilt {\CDO} versions are available in online Unix repositories, and you can install them by typing on the Unix terminal
\begin{verbatim}
    apt-get install cdo
\end{verbatim}
Note that prebuilt libraries do not offer the most recent version, and their version might vary with the Unix system (see table below). It is recommended to build from the source or Conda environment for an updated version or a customised setting. \\ 
% \vspace{2mm}
% \hspace{2cm}
\begin{table}
	\centering
\begin{tabular}[c]{|>{\columncolor{pcolor1}}l|>{\columncolor{pcolor1}}c|c|}
	\hline
	\rowcolor{pcolor2}
	%\cellcolor{pcolor2} >{\columncolor{pcolor1}}
	\multicolumn{2}{|c|}{Unix OS}& {\CDO}  Version \\ \hline
	&  11 (Bullseye) & 1.9.10-1 \\ 
	&  10 (Buster) & 1.9.6-1 \\ 
	\multirow{-3}{*}{Debian}&  Sid & 2.0.6-2 \\ \hline
	&  13 & 2.0.6 \\ 
	\multirow{-2}{*}{FreeBSD}&  12 & 2.0.6 \\ \hline
	&  Leap 15.3 & 2.0.6 \\ 
	\multirow{-2}{*}{openSUSE}&  Tumbleweed & 2.0.6-1 \\ \hline
	&  18.04 LTS&1.9.3 \\ 
	& 20.04 LTS&1.9.9\\ 
	\multirow{-3}{*}{Ubuntu}&  22.04 LTS &  2.0.4-1 \\ \hline
	
\end{tabular}
\end{table}
\subsubsection{Building from sources}

{\CDO} uses the GNU configure and build system for compilation.
The only requirement is a working ISO C++17 and C11 compiler.

%First go to the \href{https://www.mpimet.mpg.de/cdo/download.html}{\texttt{download}} page
%(\texttt{https://www.mpimet.mpg.de/\\,cdo/download.html}) to get the latest distribution,
%First go to the \href{https://www.mpimet.mpg.de/cdo}{\texttt{download}} page
%(\texttt{https://www.mpimet.mpg.de/cdo}) to get the latest distribution,
First go to the \href{https://code.mpimet.mpg.de/projects/cdo}{\texttt{download}} page
(\texttt{https://code.mpimet.mpg.de/projects/cdo}) to get the latest distribution,
if you do not have it yet.

To take full advantage of {\CDO} features the following additional libraries should be installed:

\begin{itemize}
	\item Unidata \href{https://www.unidata.ucar.edu/software/netcdf}{NetCDF} library
	(\texttt{https://www.unidata.ucar.edu/software/netcdf})
	version 3 or higher. \\
	This library is needed to process NetCDF \cite{NetCDF} files with {\CDO}. 
	\item ECMWF \href{https://software.ecmwf.int/wiki/display/ECC/ecCodes+Home}{ecCodes} library
	(\texttt{https://software.ecmwf.int/wiki/display/ECC/ecCodes+Home})
	version 2.3.0 or higher.
	This library is needed to process GRIB2 files with {\CDO}. 
	\item HDF5 \href{https://www.hdfgroup.org/doc_resource/SZIP}{szip} library
	(\texttt{https://www.hdfgroup.org/doc\_resource/SZIP})
	version 2.1 or higher. \\
	This library is needed to process szip compressed GRIB \cite{GRIB} files with {\CDO}. 
	\item \href{https://www.hdfgroup.org}{HDF5} library
	(\texttt{https://www.hdfgroup.org})
	version 1.6 or higher. \\
	This library is needed to import  CM-SAF \cite{CM-SAF} HDF5 files with the {\CDO}
	operator \textbf{import\_cmsaf}. 
	\item \href{https://proj.org}{PROJ} library
	(\texttt{https://proj.org})
	version 5.0 or higher. \\
	This library is needed to convert Sinusoidal and Lambert Azimuthal Equal Area coordinates
	to geographic coordinates, for e.g. remapping. 
	\item \href{https://software.ecmwf.int/wiki/display/MAGP/Magics}{Magics} library
	(\texttt{https://software.ecmwf.int/wiki/display/MAGP/Magics})
	version 2.18 or higher. \\
	This library is needed to create contour, vector and graph plots with {\CDO}.
\end{itemize}

% {\CDO} is a multi-threaded application. Therefore all the above libraries should be compiled thread safe. 
% Using non-threadsafe libraries could cause unexpected errors!

%This section is divided into the following sections:
%<ul>
%<li>\ref install_src_unix  "Compiling from source on Unix"
%<li>\ref install_bin_unix  "Installing the binaries on Unix"
%<li>\ref build_tools       "Tools used to develop CDO"
%</ul>


\subsubsection*{Compilation}
\begin{addmargin}[25pt]{0pt}

Compilation is done by performing the following steps:

\begin{enumerate}
\item Unpack the archive, if you haven't done that yet:
   
\begin{verbatim}
    gunzip cdo-$VERSION.tar.gz    # uncompress the archive
    tar xf cdo-$VERSION.tar       # unpack it
    cd cdo-$VERSION
\end{verbatim}
%$
\item Run the configure script:
 
\begin{verbatim}
    ./configure
\end{verbatim}

\begin{itemize}
\item Optionaly with NetCDF \cite{NetCDF} support:
 
\begin{verbatim}
    ./configure --with-netcdf=<NetCDF root directory>
\end{verbatim}

\item and with ecCodes:
 
\begin{verbatim}
    ./configure --with-eccodes=<ecCodes root directory>
\end{verbatim}

%%You have to specify also the location of the SZLIB if HDF5 was build with SZLIB support.\\
%%
%%\item To enable szip \cite{szip} support add:
%% 
%%\begin{verbatim}
%%    --with-szlib=<SZLIB root directory>
%%\end{verbatim}

\end{itemize}

For an overview of other configuration options use

\begin{verbatim}
    ./configure --help
\end{verbatim}

\item Compile the program by running make:

\begin{verbatim}
    make
\end{verbatim}

\end{enumerate}

The program should compile without problems and the binary (\texttt{cdo}) 
should be available in the \texttt{src} directory of the distribution.

\end{addmargin}


\subsubsection*{Installation}
\begin{addmargin}[25pt]{0pt}

After the compilation of the source code do a \texttt{make install},
possibly as root if the destination permissions require that.

\begin{verbatim}
    make install
\end{verbatim} 

The binary is installed into the directory \texttt{$<$prefix$>$/bin}.
\texttt{$<$prefix$>$} defaults to \texttt{/usr/local} but can be changed with 
the \textsl{-{}-prefix} option of the configure script. 

Alternatively, you can also copy the binary from the \texttt{src} directory
manually to some \texttt{bin} directory in your search path.

\end{addmargin}


\subsubsection{Conda}
Conda is an open-source package manager and environment management system for various languages (Python, R, etc.). Conda is installed via Anaconda or Miniconda. Unlike Anaconda, miniconda is a lightweight conda distribution. They can be dowloaded from the main conda Website (\url{https://conda.io/projects/conda/en/latest/user-guide/install/linux.html}) or on the terminal 

\begin{verbatim}
    wget https://repo.anaconda.com/archive/Anaconda3-2021.11-Linux-x86_64.sh
    bash Anaconda3-2021.11-Linux-x86_64.sh
    source ~/.bashrc
\end{verbatim} 

and 
\begin{verbatim}
    wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
    sh Miniconda3-latest-Linux-x86_64.sh
\end{verbatim} 
Upon setting your conda environment, you can install {\CDO} using conda
\begin{verbatim}
    conda install cdo
    conda install python-cdo
\end{verbatim}
\subsection{MacOS}
Among the MacOS package managers, {\CDO} can be installed from Homebrew and Macports. 
The installation via Homebrew is straight forward process on the terminal
\begin{verbatim}
    brew install cdo
\end{verbatim}
Similarly, Macports
\begin{verbatim}
    port install cdo
\end{verbatim}
In contrast to Homebrew, Macport allows you to enable  GRIB2, szip compression and Magics++ graphic in {\CDO} installation.
\begin{verbatim}
    port install cdo +grib_api +magicspp +szip
\end{verbatim}
In addition, you could also set {\CDO} via Conda as Unix. You can follow this \href{https://code.mpimet.mpg.de/projects/cdo/wiki/MacOS_Platform}{\texttt{tutorial}}  to install anaconda or miniconda in your computer (\url{https://conda.io/projects/conda/en/latest/user-guide/install/macos.html}). Then, you can install cdo by
\begin{verbatim}
    conda install -c conda-forge cdo
\end{verbatim}
\subsection{Windows} 
Currently, {\CDO} is not supported in Windows system and the binary is not available in the windows conda repository. Therefore, {\CDO}  needs to be set in a virtual environment. Here, it covers the installation of {\CDO} using Windows Subsystem Linux (WSL) and virtual machines.\\   
\subsubsection{WSL}
WSL emulates Unix in your Windows system. Then, you can install Unix libraries and software such as  {\CDO} or the linux conda distribution in your computer. Also, it allows you to directly share your files between your Windows and the WSL environment. However, more complex functions that require a graphic interface are not allowed.\par
In Windows 10 or newer, WSL can be readily set in your cmd by typing
\begin{verbatim}
    wsl --install
\end{verbatim}
This command will install, by default, Ubuntu 20.04 in WSL2. You could also choose a different system from this list.
\begin{verbatim}
    wsl -l -o
\end{verbatim}  
Then, you can install your WSL environment as
\begin{verbatim}
    wsl --install -d NAME
\end{verbatim} 
\subsubsection{Virtual machine}
Virtual machines can emulate different operative systems in your computer. Virtual machines are guest computers mounted inside your host computer. You can set a Linux distribution in your Windows device in this particular case. The advantages of Virtual machines to WSL are the graphical interface and the fully operational Linux system. You can follow any tutorial on the internet such as this one \\

\url{https://ubuntu.com/tutorials/how-to-run-ubuntu-desktop-on-a-virtual-machine-using-virtualbox#1-overview}\\
 
Finally, you can install {\CDO} following any method listed in the section \ref{sec:unix}.
