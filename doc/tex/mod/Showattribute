@BeginModule
@Name      = Showattribute
@Title     = Show attributes
@Section   = Information
@Class     = Information
@Arguments = infile
@Operators = showattribute

@BeginDescription
This operator prints the attributes of the data variables of a dataset.

Each attribute has the following structure:

  [@bold{var_nm}@][@bold{att_nm}]

@IfMan
   var_nm  Variable name (optional). Example: pressure
   att_nm  Attribute name (optional). Example: units
@EndifMan
@IfDoc
\begin{defalist}{\textbf{var_nm}}
\item[\ \ \ \textbf{var_nm}\ \ \hfill]  Variable name (optional). Example: pressure
\item[\ \ \ \textbf{att_nm}\ \ \hfill]  Attribute name (optional). Example: units
\end{defalist}
@EndifDoc

The value of @bold{var_nm} is the name of the variable containing the attribute (named @bold{att_nm}) that
you want to print. Use wildcards to print the attribute @bold{att_nm} of more than one variable.
A value of @bold{var_nm} of '@bold{*}' will print the attribute @bold{att_nm} of all data variables.
If @bold{var_nm} is missing then @bold{att_nm} refers to a global attribute.

The value of @bold{att_nm} is the name of the attribute you want to print. Use wildcards to print more than
one attribute. A value of @bold{att_nm} of '@bold{*}' will print all attributes.
@EndDescription
@EndModule

@BeginOperator_showattribute
@Title     = Show a global attribute or a variable attribute
@Parameter = [attributes]
@EndOperator

@BeginParameter
@Item = attributes
STRING  Comma-separated list of attributes. 
@EndParameter
