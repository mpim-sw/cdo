@BeginModule
@NewPage
@Name      = Healpix
@Title     = Change healpix resolution
@Section   = Miscellaneous
@Class     = Miscellaneous
@Arguments = infile outfile
@Operators = hpdegrade hpupgrade

@BeginDescription
Degrade or upgrade the resolution of a healpix grid.
@EndDescription
@EndModule


@BeginOperator_hpdegrade
@Title     = Degrade healpix
@Parameter = parameter

@BeginDescription
Degrade the resolution of a healpix grid. The value of the target pixel is the mean of the source pixels.
@EndDescription
@EndOperator


@BeginOperator_hpupgrade
@Title     = Upgrade healpix
@Parameter = parameter

@BeginDescription
Upgrade the resolution of a healpix grid. The values of the target pixels is the value of the source pixel.
@EndDescription
@EndOperator


@BeginParameter
@Item = nside
INTEGER The nside of the target healpix, must be a power of two [default: same as input].
@Item = order
STRING  Pixel ordering of the target healpix ('nested' or 'ring').
@Item = power
FLOAT   If non-zero, divide the result by (nside[in]/nside[out])**power. power=-2 keeps the sum of the map invariant.
@EndParameter
