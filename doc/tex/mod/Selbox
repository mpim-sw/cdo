@BeginModule
@NewPage
@Name      = Selbox
@Title     = Select a box
@Section   = Selection
@Class     = Selection
@Arguments = infile outfile
@Operators = sellonlatbox selindexbox

@BeginDescription
Selects grid cells inside a lon/lat or index box.
@EndDescription
@EndModule


@BeginOperator_sellonlatbox
@Title     = Select a longitude/latitude box
@Parameter = lon1 lon2 lat1 lat2

@BeginDescription
Selects grid cells inside a lon/lat box. The user must specify the longitude and latitude of the edges of the box.
Only those grid cells are considered whose grid center lies within the lon/lat box.
For rotated lon/lat grids the parameters must be specified in rotated coordinates.
@EndDescription
@EndOperator


@BeginOperator_selindexbox
@Title     = Select an index box
@Parameter = idx1 idx2 idy1 idy2

@BeginDescription
Selects grid cells within an index box. The user must specify the indices of the edges of the box.
The index of the left edge can be greater then the one of the right edge. Use negative indexing to
start from the end. The input grid must be a regular lon/lat or a 2D curvilinear grid.
@EndDescription
@EndOperator


@BeginParameter idx1
@Item = lon1
FLOAT    Western longitude in degrees
@Item = lon2
FLOAT    Eastern longitude in degrees
@Item = lat1
FLOAT    Southern or northern latitude in degrees
@Item = lat2
FLOAT    Northern or southern latitude in degrees
@Item = idx1
INTEGER  Index of first longitude (1 - nlon)
@Item = idx2
INTEGER  Index of last longitude (1 - nlon)
@Item = idy1
INTEGER  Index of first latitude (1 - nlat)
@Item = idy2
INTEGER  Index of last latitude (1 - nlat)
@EndParameter


@BeginExample
To select the region with the longitudes from 30W to 60E and latitudes from 30N to 80N from all input fields use:
@BeginVerbatim
   cdo sellonlatbox,-30,60,30,80 infile outfile
@EndVerbatim
If the input dataset has fields on a Gaussian N16 grid, the same box can be selected with @oper{selindexbox} by:
@BeginVerbatim
   cdo selindexbox,60,11,3,11 infile outfile
@EndVerbatim
@EndExample
