@BeginModule
@NewPage
@Name      = Copy
@Title     = Copy datasets
@Section   = File operations
@Class     = File operation
@Arguments = infiles outfile
@Operators = copy clone cat

@BeginDescription
This module contains operators to copy, clone or concatenate datasets.
@file{infiles} is an arbitrary number of input files. All input files need to have 
the same structure with the same variables on different timesteps.
@EndDescription
@EndModule


@BeginOperator_copy
@Title     = Copy datasets

@BeginDescription
Copies all input datasets to @file{outfile}. 
@EndDescription
@EndOperator


@BeginOperator_clone
@Title     = Clone datasets

@BeginDescription
Copies all input datasets to @file{outfile}. In contrast to the copy operator, clone tries
not to change the input data. GRIB records are neither decoded nor decompressed.
@EndDescription
@EndOperator

@BeginOperator_cat
@Title     = Concatenate datasets

@BeginDescription
Concatenates all input datasets and appends the result to the end 
of @file{outfile}. If @file{outfile} does not exist it will be created.
@EndDescription
@EndOperator


@BeginExample
To change the format of a dataset to NetCDF use:
@BeginVerbatim
   cdo -f nc copy infile outfile.nc
@EndVerbatim
Add the option '-r' to create a relative time axis,
as is required for proper recognition by GrADS or Ferret:
@BeginVerbatim
   cdo -r -f nc copy infile outfile.nc
@EndVerbatim
To concatenate 3 datasets with different timesteps of the same variables use:
@BeginVerbatim
   cdo copy infile1 infile2 infile3 outfile
@EndVerbatim
If the output dataset already exists and you wish to extend it 
with more timesteps use:
@BeginVerbatim
   cdo cat infile1 infile2 infile3 outfile
@EndVerbatim
@EndExample
