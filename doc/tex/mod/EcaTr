@BeginModule
@NewPage
@Name      = EcaTr
@Title     = Tropical nights index per time period
@Section   = Climate indices
@Class     = Climate index
@Arguments = infile outfile
@Operators = eca_tr etccdi_tr

@BeginDescription
Let @file{infile} be a time series of the daily minimum temperature TN, then the number of days where 
TN > @math{T} is counted. The number @math{T} is an optional parameter with default @math{T} = 20@celsius. 
Note that TN have to be given in units of Kelvin, whereas @math{T} have to be given in degrees Celsius.
Parameter is a comma-separated list of "key=values" pairs.
@EndDescription
@EndModule


@BeginOperator_eca_tr
@Title     = Tropical nights index per time period
@Parameter = [T] [params]
@BeginDescription
The operator counts over the entire time series.
The date information of a timestep in @file{outfile} is the date of
the last contributing timestep in @file{infile}.
@EndDescription
@EndOperator

@BeginOperator_etccdi_tr
@Title     = Tropical nights index per time period
@Parameter = [T] [params]
@BeginDescription
The default output frequency is yearly.
The date information of a timestep in @file{outfile} is the mid of
the frequency interval.
@EndDescription
@EndOperator


@BeginParameter
@Item = T
FLOAT   Temperature threshold (unit: @celsius; default: T = 20@celsius)
@Item = freq
STRING  Output frequency (year, month)
@EndParameter


@BeginExample
To get the number of tropical nights of a time series of daily minimum temperatures use:
@BeginVerbatim
   cdo eca_tr tnfile outfile
@EndVerbatim
@EndExample
