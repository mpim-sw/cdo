@BeginModule
@NewPage
@Name      = Merstat
@Title     = Meridional statistics
@Section   = Statistical values
@Class     = Statistic
@Arguments = infile outfile
@Operators = mermin mermax merrange mersum mermean meravg merstd merstd1 mervar mervar1 merskew merkurt mermedian merpctl

@BeginDescription
This module computes meridional statistical values of the input fields.
Depending on the chosen operator, the meridional minimum, maximum, range, sum, average, standard deviation, variance,
skewness, kurtosis, median or a certain percentile of the field is written to @file{outfile}.
Operators of this module require all variables on the same regular lon/lat grid.
@EndDescription
@EndModule


@BeginOperator_mermin
@Title     = Meridional minimum

@BeginDescription
For every longitude the minimum over all latitudes is computed.
@EndDescription
@EndOperator


@BeginOperator_mermax
@Title     = Meridional maximum

@BeginDescription
For every longitude the maximum over all latitudes is computed.
@EndDescription
@EndOperator


@BeginOperator_merrange
@Title     = Meridional range

@BeginDescription
For every longitude the range over all latitudes is computed.
@EndDescription
@EndOperator


@BeginOperator_mersum
@Title     = Meridional sum

@BeginDescription
For every longitude the sum over all latitudes is computed.
@EndDescription
@EndOperator


@BeginOperator_mermean
@Title     = Meridional mean

@BeginDescription
For every longitude the area weighted mean over all latitudes is computed.
@EndDescription
@EndOperator


@BeginOperator_meravg
@Title     = Meridional average

@BeginDescription
For every longitude the area weighted average over all latitudes is computed.
@EndDescription
@EndOperator


@BeginOperator_mervar
@Title     = Meridional variance

@BeginDescription
For every longitude the variance over all latitudes is computed. Normalize by n.
@EndDescription
@EndOperator


@BeginOperator_mervar1
@Title     = Meridional variance (n-1)

@BeginDescription
For every longitude the variance over all latitudes is computed. Normalize by (n-1).
@EndDescription
@EndOperator


@BeginOperator_merstd
@Title     = Meridional standard deviation

@BeginDescription
For every longitude the standard deviation over all latitudes is computed. Normalize by n.
@EndDescription
@EndOperator


@BeginOperator_merstd1
@Title     = Meridional standard deviation (n-1)

@BeginDescription
For every longitude the standard deviation over all latitudes is computed. Normalize by (n-1).
@EndDescription
@EndOperator


@BeginOperator_merskew
@Title     = Meridional skewness

@BeginDescription
For every longitude the skewness over all latitudes is computed.
@EndDescription
@EndOperator


@BeginOperator_merkurt
@Title     = Meridional kurtosis

@BeginDescription
For every longitude the kurtosis over all latitudes is computed.
@EndDescription
@EndOperator


@BeginOperator_mermedian
@Title     = Meridional median

@BeginDescription
For every longitude the median over all latitudes is computed.
@EndDescription
@EndOperator


@BeginOperator_merpctl
@Title     = Meridional percentiles
@Parameter = p

@BeginDescription
For every longitude the pth percentile over all latitudes is computed.
@EndDescription
@EndOperator


@BeginParameter
@Item = p
FLOAT  Percentile number in {0, ..., 100}
@EndParameter


@BeginExample
To compute the meridional mean of all input fields use:
@BeginVerbatim
   cdo mermean infile outfile
@EndVerbatim

To compute the 50th meridional percentile (median) of all input fields use:
@BeginVerbatim
   cdo merpctl,50 infile outfile
@EndVerbatim
@EndExample
