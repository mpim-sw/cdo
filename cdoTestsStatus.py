import glob
import os
import sys

class bcolors:
    if sys.stdout.isatty():
        HEADER    = '\033[95m'
        OKBLUE    = '\033[94m'
        OKGREEN   = '\033[92m'
        WARNING   = '\033[93m'
        FAIL      = '\033[91m'
        ENDC      = '\033[0m'
        BOLD      = '\033[1m'
        UNDERLINE = '\033[4m'
    else:
        HEADER    = ''
        OKBLUE    = ''
        OKGREEN   = ''
        WARNING   = ''
        FAIL      = ''
        ENDC      = ''
        BOLD      = ''
        UNDERLINE = ''

#===========STATISTICS=====================
numModules                   = 0 #
numSubModules                = 0 #
numOperators                 = 0 #
numDedicatedTestFiles        = 0 #
numTestFiles                 = 0 #
numNonModuleRelatedTestFiles = 0 #
percentageOfTestedModules    = 0
percentageOfTestedOperators  = 0
numTestedOperators           = 0
numUntestedOperators         = 0
numModulesTested             = 0
numTestedSubModules          = 0
numTestedModules             = 0
#==========================================


moduleFiles = glob.glob("src/[A-Z]*.cc")
moduleFiles = [ os.path.basename(x).replace(".cc","") for x in moduleFiles ]
testFiles = glob.glob("test/*.test.in")
testFiles = [ os.path.basename(x).replace(".test.in","") for x in testFiles ]
def scanModuleOperators():
    global numModules
    global numSubModules
    global numOperators
    d = {}
    file = open("src/module_definitions.h")
    fileContests = [ x for x in file ]
    currentLine = ""
    currentMod = ""
    for x in fileContests:
        currentLine += x
        if "(void *argument)" in currentLine:
            start = currentLine.find(" *") + 2
            end = currentLine.find("(")
            currentMod=currentLine[start:end].replace(" ","")
            d[currentMod] = {}
            currentLine = ""
            numModules+=1

        elif "define" in currentLine and not len(currentMod) == 0:
            if currentLine.find('}') != -1:
                start = currentLine.find("{") + 1
                end = currentLine.find("}")
                subOpername = currentLine[:start-1].replace("#define ",'').replace("Operators",'').strip()
                opers=currentLine[start:end].replace(" ","")
                token = opers.split(',')
                token = [ x.replace("\"",'') for x in token]
                numOperators += len(token)
                d[currentMod].update({subOpername : token})
                currentLine = ""
                numSubModules += 1
        else:
            currentLine = ""
    return d
# d is a dictionary with all

def scanFilesForOperator():
    global numDedicatedTestFiles
    global numNonModuleRelatedTestFiles
    global numTestFiles
    files = glob.glob("test/*.test.in")
    numTestFiles = len(files)
    dict = {}
    for file in files:
        f = open(file)
        opertypes=[]
        for l in f:
            if "OPERTYPES=" in l:
                opertypes = l[l.find("\"") + 1 : -2].replace("\"","").split(" ")
            if "STATS=" in l:
                temp = l[l.find("\"") + 1 : -2].replace("\"","").split(" ")
                if len(opertypes) > 0:
                    dict[file] = []
                    stats = []
                    for x in opertypes:
                        for y in temp:
                            dict[file].append(x+y)
                    opertypes=[]
                else:
                    if file in dict:
                        for x in temp:
                            dict[file].append(x)
                    else:
                        dict[file] = temp
            if "OPERATORS=" in l:
                temp = l[l.find("\"") + 1 : -2].replace("\"","").split(" ")
                if file in dict:
                    for x in temp:
                        dict[file].append(x)
                else:
                    dict[file] = temp

        f.close()

    numDedicatedTestFiles = len(dict)
    numNonModuleRelatedTestFiles = numTestFiles - numDedicatedTestFiles
    return dict

def scanForOperators(d,opersInFiles):
    d2={}
    for mod in d:
        for oper in d[mod]:
            d2[oper] = []
            for file in opersInFiles:
                if oper in opersInFiles[file]:
                    d2[oper].append(str(file))
    return d2 ,0

class res:
    def __init__(self,p_modName,p_moduleData,verbose,onlyTested):

        self.errStateCol = {0:"", 1:bcolors.WARNING, 2:bcolors.FAIL}
        self.modName = p_modName
        self.verbose = verbose
        self.onlyTested = onlyTested
        self.resStr = ""
        self.errStr = ""
        self.errStateMod = [0,0,0,0,0,0]
        self.errStateOpers= []
        self.modNames = [ x for x in p_moduleData]
        self.mods = [ p_moduleData[x] for x in p_moduleData ]
        self.numSubMods = len(self.mods)
        self.numOperPerSub = [ len(x) for x in self.mods]
        self.numFoundOperPerSub = [0 for x in self.mods]
        self.numOpers= sum(self.numOperPerSub)

    def containsOperator(self, operName):
        for x in self.mods:
            if operName in self.mods[x]:
                return True
        return False

    def getOperList(self):
        list = []
        for x in self.mods:
            list += x
        return list

    def check(self,p_moduleData,opers):
        global numTestedOperators
        global numTestedSubModules
        global numUntestedOperators
        self.d2, self.numMissingOperators = scanForOperators(p_moduleData,opers) 
        if self.modName not in testFiles:
            self.errStateMod[2] = 2
        i = 0
        for subModule in self.mods:
            errStr = ""
            for operator in subModule:
                if len(self.d2[operator]) > 0:
                    files = [os.path.basename(x) for x in self.d2[operator]]
                    errStr+= "\t\t" + bcolors.OKGREEN + operator.ljust(14) +bcolors.ENDC+", ".join(files)+"\n"
                    self.numFoundOperPerSub[i]+=1
                    numTestedOperators +=1
                else:
                    errStr+= "\t\t" +  bcolors.FAIL + operator.ljust(15) + bcolors.ENDC+"\n"
                    self.errStateMod[0] = 1


            if(self.errStateMod[0] == 0):
                self.errStr += ("\t"  +(bcolors.OKGREEN + self.modNames[i]).ljust(27)+"all submodule operators tested"+ bcolors.ENDC+"\n")
            else:
               if(self.numFoundOperPerSub[i] == 0):
                   self.errStr +=bcolors.FAIL+("\t" +self.modNames[i].ljust(22))+"all submodule tests missing" + bcolors.ENDC+"\n"
                   if(self.verbose):
                       self.errStr += errStr
               elif (self.numFoundOperPerSub[i] == self.numOperPerSub[i]):
                   self.errStr +=bcolors.OKGREEN+("\t" +self.modNames[i].ljust(22))+ "all submodule operators tested" + bcolors.ENDC+"\n"
                   self.errStr += errStr
                   numTestedSubModules += 1
               else:
                   self.errStr +=bcolors.WARNING+("\t" +self.modNames[i].ljust(22))+ "missing operator tests" + bcolors.ENDC+"\n"
                   self.errStr += errStr
            i+=1
            numUntestedOperators = numOperators - numTestedOperators

    def write(self):
        global numModulesTested
        if self.errStateMod[2] != 0:
            self.errStr = self.modName.ljust(30) + self.errStateCol[self.errStateMod[2]] + "missing dedicated testfile" + bcolors.ENDC+"\n" + self.errStr
        if self.errStateMod[2] == 0:
            self.modName.ljust(30) + bcolors.OKGREEN + "OK - all operators tested" + bcolors.ENDC + "\n"
            err =  self.modName.ljust(30) + bcolors.OKGREEN + "dedicated testfile exitst" + bcolors.ENDC + "\n"
            if(sum(self.errStateMod) == 0):
                self.errStr =self.modName.ljust(30) + bcolors.OKGREEN + "OK - all operators tested" + bcolors.ENDC + "\n"
                numModulesTested += 1
            else:
                self.errStr = err+self.errStr

        if(not self.onlyTested or sum(self.errStateMod)==0):
            print(self.errStr)
            print("=" * 80)

def run():
    argv = sys.argv[1:]
    numOption = 0
#OPTIONS DEF ---------------------------
    verbose = False
    onlyTested = False
    reduce = False

#OPTION HANDLING -----------------------

    if "-v" in argv:
        verbose = True
        numOption += 1
    if "-r" in argv:
        onlyTested = True
        numOption += 1

    if (len(argv) - numOption > 0):
        reduce = True

#OPTIONS END ---------------------------

#Gather present operators in testfiles
    foundOperators = scanFilesForOperator()
#Gater modules from src/module_definitions.h
    d = scanModuleOperators()

#check for tests
    for module in d:
        r = res(module,d[module],verbose,onlyTested)
        execute = True
        if reduce:
            if not len(set(r.getOperList()) & set(argv)) > 0 and not len(set(argv) & set([x for x in d[module]])) > 0:
                execute = False
        if module in argv or execute:
            r.check(d[module],foundOperators)
            r.write()

#print statistics
    if not reduce:
        percentageOfTestedModules = 100.0/ numModules * numModulesTested
        percentageOfTestedOperators = 100.0 / numOperators * numTestedOperators
        print("numModules                       ", numModules)
        print("numSubModules                    ", numSubModules)
        print("numOperators                     ", numOperators)
        print("numDedicatedTestFiles            ", numDedicatedTestFiles)
        print("numTestFiles                     ", numTestFiles)
        print("numNonModuleRelatedTestFiles     ", numNonModuleRelatedTestFiles)
        print("percentageOfTestedModules         %.2f%%  " % percentageOfTestedModules)
        print("percentageOfTestedOperators       %.2f%% " % percentageOfTestedOperators)
        print("numTestedOperators               ", numTestedOperators)
        print("numUntestedOperators             ", numUntestedOperators)

run()

