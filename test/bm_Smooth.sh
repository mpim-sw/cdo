#!/bin/bash
set -e
#
if [ $HOSTNAME == "hama.fritz.box" ] ; then
    CDO=/Users/m214003/cdt/work/cdo/build/clang/src/cdo
else
    CDO=/home/m/m214003/local/bin/cdotest
fi
# cdo-2.0.4 on levante 11.03.2022
#
$CDO -f nc4 topo,icor2b8 topor2b8.nc
for NP in 256 128 64 32 16 8 4 2 1; do $CDO -P $NP smooth,maxpoints=1000 topor2b8.nc smooth_$NP; done
# cdo    smooth: Processed 5242880 values from 1 variable over 1 timestep [5.06s 692MB 256thread].
# cdo    smooth: Processed 5242880 values from 1 variable over 1 timestep [4.61s 691MB 128thread].
# cdo    smooth: Processed 5242880 values from 1 variable over 1 timestep [5.49s 687MB 64thread].
# cdo    smooth: Processed 5242880 values from 1 variable over 1 timestep [8.53s 685MB 32thread].
# cdo    smooth: Processed 5242880 values from 1 variable over 1 timestep [12.87s 685MB 16thread].
# cdo    smooth: Processed 5242880 values from 1 variable over 1 timestep [24.59s 685MB 8thread].
# cdo    smooth: Processed 5242880 values from 1 variable over 1 timestep [44.25s 684MB 4thread].
# cdo    smooth: Processed 5242880 values from 1 variable over 1 timestep [83.53s 684MB 2thread].
# cdo    smooth: Processed 5242880 values from 1 variable over 1 timestep [157.40s 684MB 1thread].
#
$CDO -f nc4 topo,icor2b9 topor2b9.nc
for NP in 256 128 64 32 16 8 4 2 1; do $CDO -P $NP smooth,maxpoints=3000 topor2b9.nc smooth_$NP; done
# cdo    smooth: Processed 20971520 values from 1 variable over 1 timestep [82.18s 2585MB 256thread].
# cdo    smooth: Processed 20971520 values from 1 variable over 1 timestep [87.97s 2570MB 128thread].
# cdo    smooth: Processed 20971520 values from 1 variable over 1 timestep [121.66s 2563MB 64thread].
# cdo    smooth: Processed 20971520 values from 1 variable over 1 timestep [218.15s 2558MB 32thread].
# cdo    smooth: Processed 20971520 values from 1 variable over 1 timestep [399.30s 2558MB 16thread].
# cdo    smooth: Processed 20971520 values from 1 variable over 1 timestep [758.98s 2557MB 8thread].
# cdo    smooth: Processed 20971520 values from 1 variable over 1 timestep [1517.45s 2556MB 4thread].
# cdo    smooth: Processed 20971520 values from 1 variable over 1 timestep [3037.99s 2556MB 2thread].
# cdo    smooth: Processed 20971520 values from 1 variable over 1 timestep [5840.25s 2554MB 1thread].
