#include "bandit/bandit/bandit.h"
#include <iostream>
#include <stdlib.h>

#include "../../src/cdo_module.h"
#include "../../src/factory.h"
#include "../../src/modules.h"
#include "../../src/param_conversion.h"
#include "../../src/process.h"

using namespace snowhouse;

#include "test_module_list.h"
class OptionsModule : public DummyProcess {
  public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module = {
    .name = "TestModule",
    .operators = { { "oper1-1", 0, 1, "constantvalue", dummy_help },
        { "oper1-2", 0, 1, "constantvalue", dummy_help } },
    .aliases = { Alias("alias1-1", "oper1-1") },
    .mode = 0,          // Module mode: 0:intern 1:extern
    .number = CDI_BOTH, // Allowed number type
    .constraints = { 1, 1, NoRestriction },
    .arguments = {
        required("required", convert<std::string>),
        optional("optional", convert<double>),
        optional(
            "lambda", [](std::string val) -> size_t { return val.size(); }, "exclusive"),
        optional(
            "exclusive", [](std::string val) -> size_t { return val.size(); }, "lambda"),
    },
  };
  inline static RegisterEntry<TestModule> registration
      = RegisterEntry<TestModule>(module);

  void required_and_optional(
      size_t& lam,
      std::string& required,
      double& optional)
  {
    parse_arguments();

    get_argument("required", required);
    get_argument("optional", optional);
    get_argument("lambda", lam);
  }
  void required_missing()
  {
    parse_arguments();

    std::string required;
    get_argument("required", required);
  }
  void excl()
  {
    parse_arguments();

    size_t lambda;
    size_t excl;
    get_argument("lambda", lambda);
    get_argument("exclusive", excl);
  }
  void no_value()
  { /* TODO */

    parse_arguments();
  }

  void bad_any_cast()
  {
    parse_arguments();

    size_t optional;
    get_argument("optional", optional);
  }
};
void testExit(std::string msg)
{
  throw std::invalid_argument(msg);
}
go_bandit([]() {
  // setup
  cdo::progname = "cdo_operator_args_test";
  cdo::set_exit_function(testExit);

  // tests
  bandit::describe("the operator arguments",
      [&]() {
        bandit::it("throw an error if a required arg is missing", [&]() {
          int p_ID = 0;
          std::string operName = "oper1-1";
          std::vector<std::string> operatorArguments = { "optional=1.337" };
          auto new_process = std::make_shared<OptionsModule>(0, operName, operatorArguments, OptionsModule::module);

          AssertThrows(std::invalid_argument, new_process->required_missing());
          AssertThat(LastException<std::invalid_argument>().what(),
              Contains("required"));
        });
        bandit::it("throw an error no value was given", [&]() {
          int p_ID = 0;
          std::string operName = "oper1-1";
          std::vector<std::string> operatorArguments = { "optional=" };
          auto new_process = std::make_shared<OptionsModule>(0, operName, operatorArguments, OptionsModule::module);

          AssertThrows(std::invalid_argument, new_process->no_value());
          AssertThat(LastException<std::invalid_argument>().what(),
              Contains("no value"));
        });

        bandit::it("does not allow mutualy exclusive args", [&]() {
          int p_ID = 0;
          std::string operName = "oper1-1";
          std::vector<std::string> operatorArguments = { "lambda=1234", "exclusive=1234" };
          auto new_process = std::make_shared<OptionsModule>(0, operName, operatorArguments, OptionsModule::module);

          AssertThrows(std::invalid_argument, new_process->excl());
          AssertThat(LastException<std::invalid_argument>().what(),
              Contains("can not be combined with any of"));
        });
        bandit::it("handles bad any casts and gives a useful error", [&]() {
          int p_ID = 0;
          std::string operName = "oper1-1";
          std::vector<std::string> operatorArguments = { "optional=1335" };
          auto new_process = std::make_shared<OptionsModule>(0, operName, operatorArguments, OptionsModule::module);

          AssertThrows(std::invalid_argument, new_process->bad_any_cast());
          AssertThat(LastException<std::invalid_argument>().what(),
              Contains("Mismatch while getting argument"));
        });

        bandit::it("have the right values", [&]() {
          int p_ID = 0;
          std::string operName = "oper1-1";
          std::vector<std::string> operatorArguments = { "required=test", "optional=1.337", "lambda=lam" };
          auto new_process = std::make_shared<OptionsModule>(0, operName, operatorArguments, OptionsModule::module);

          size_t lam;
          std::string required;
          double optional;

          new_process->required_and_optional(lam, required, optional);

          AssertThat(lam, Equals(3));
          AssertThat(required, Equals("test"));
          AssertThat(optional, Equals(1.337));
        });
      });
});

int main(int argc, char** argv)
{
  std::vector<char*> argv_v(argv, argv + argc);
  std::string reporter = "--reporter=spec";
  argv_v.push_back(&reporter[0]);
  int result = bandit::run(argc + 1, argv_v.data());

  return result;
}
