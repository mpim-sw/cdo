#ifndef TEST_MODULE_LIST_H
#define TEST_MODULE_LIST_H
#include "../../src/modules.h"
#include "../../src/factory.h"
#include "../../src/process.h"
#include "../../src/cdo_module.h"

const CdoHelp dummy_help = { "dummy_help" };

class DummyProcess : public Process
{
public:
  using Process::Process;
  void init(){};
  void run(){};
  void close(){};
};

const std::string alias_name = "alias1-1";

class TestModule : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module = {
    .name = "TestModule",
    .operators = { { "oper1-1", 0, 1, "constantvalue", dummy_help },
                   { "oper1-2", 0, 1, "constantvalue", dummy_help } },
    .aliases = { Alias("alias1-1", "oper1-1") },
    .mode = 0,           // Module mode: 0:intern 1:extern
    .number = CDI_BOTH,  // Allowed number type
    .constraints = { 1, 1, NoRestriction },
  };
  inline static RegisterEntry<TestModule> registration
      = RegisterEntry<TestModule>(module);
};

class OnlyFirstModule : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module = {
    .name = "TestModule",
    .operators = { { "only_first_oper", 0, 1, "constantvalue", dummy_help } },
    .aliases = {},
    .mode = 0,           // Module mode: 0:intern 1:extern
    .number = CDI_BOTH,  // Allowed number type
    .constraints = { 1, 1, OnlyFirst },
  };
  inline static RegisterEntry<OnlyFirstModule> registration
      = RegisterEntry<OnlyFirstModule>(module);
};

class Module_in0_out1 : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module
      = { .name = "in0_out1",
          .operators = { { "in0_out1", 0, 0, nullptr, dummy_help } },
          .aliases = {},
          .mode = 1,
          .number = 0,
          .constraints = { 0, 1, NoRestriction } };

  inline static RegisterEntry<Module_in0_out1> registration
      = RegisterEntry<Module_in0_out1>(module);
};
class Module_in1_out0 : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module
      = { .name = "in1_out0",
          .operators = { { "in1_out0", 0, 0, nullptr, dummy_help } },
          .aliases = {},
          .mode = 1,
          .number = 0,
          .constraints = { 1, 0, NoRestriction } };

  inline static RegisterEntry<Module_in1_out0> registration
      = RegisterEntry<Module_in1_out0>(module);
};
class Module_in1_out1 : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module
      = { .name = "in1_out1",
          .operators = { { "in1_out1", 0, 0, nullptr, dummy_help } },
          .aliases = {},
          .mode = 1,
          .number = 0,
          .constraints = { 1, 1, NoRestriction } };

  inline static RegisterEntry<Module_in1_out1> registration
      = RegisterEntry<Module_in1_out1>(module);
};
class Module_in2_out1 : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module
      = { .name = "in2_out1",
          .operators = { { "in2_out1", 0, 0, nullptr, dummy_help } },
          .aliases = {},
          .mode = 1,
          .number = 0,
          .constraints = { 2, 1, NoRestriction } };
  inline static RegisterEntry<Module_in2_out1> registration
      = RegisterEntry<Module_in2_out1>(module);
};
class Module_in2_outObase : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module
      = { .name = "in2_outObase",
          .operators = { { "in2_outObase", 0, 0, nullptr, dummy_help } },
          .aliases = {},
          .mode = 1,
          .number = 0,
          .constraints = { 2, -1, NoRestriction } };
  inline static RegisterEntry<Module_in2_outObase> registration
      = RegisterEntry<Module_in2_outObase>(module);
};
class Module_in2_out0 : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module
      = { .name = "in2_out0",
          .operators = { { "in2_out0", 0, 0, nullptr, dummy_help } },
          .aliases = {},
          .mode = 1,
          .number = 0,
          .constraints = { 2, 0, NoRestriction } };
  inline static RegisterEntry<Module_in2_out0> registration
      = RegisterEntry<Module_in2_out0>(module);
};
class Module_inVariable_out0 : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module
      = { .name = "inVariable_out0",
          .operators = { { "inVariable_out0", 0, 0, nullptr, dummy_help } },
          .aliases = {},
          .mode = 1,
          .number = 0,
          .constraints = { -1, 0, NoRestriction } };
  inline static RegisterEntry<Module_inVariable_out0> registration
      = RegisterEntry<Module_inVariable_out0>(module);
};
class Module_inVariable_out1 : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module
      = { .name = "inVariable_out1",
          .operators = { { "inVariable_out1", 0, 0, nullptr, dummy_help } },
          .aliases = {},
          .mode = 1,
          .number = 0,
          .constraints = { -1, 1, NoRestriction } };
  inline static RegisterEntry<Module_inVariable_out1> registration
      = RegisterEntry<Module_inVariable_out1>(module);
};
class Module_files_only : public DummyProcess
{
public:
  using DummyProcess::DummyProcess;
  inline static CdoModule module
      = { .name = "files_only",
          .operators = { { "files_only", 0, 0, nullptr, dummy_help } },
          .aliases = {},
          .mode = 1,
          .number = 0,
          .constraints = { 1, 1, FilesOnly } };
  inline static RegisterEntry<Module_files_only> registration
      = RegisterEntry<Module_files_only>(module);
};

#endif
