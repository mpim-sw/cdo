#!/bin/bash
#
# examples from: https://gitlab.dkrz.de/k204210/cdo_cei/-/blob/master/cdo-cei.ipynb

CDO=cdo-2.2.0
CDO=cdo

YEARMIN=1961
YEARMAX=1990
PRDATA="pr_${YEARMIN}-${YEARMAX}.nc"
TASDATA="tas_${YEARMIN}-${YEARMAX}.nc"

# Threshold exceedances

$CDO etccdi_fd $TASDATA etccdi_fd.nc

# Create Percentiles

windowDays=5
readMethod="circular"
percentileMethod="rtype8"

$CDO ydrunmin,$windowDays,rm=$readMethod $TASDATA tas_runmin.nc
$CDO ydrunmax,$windowDays,rm=$readMethod $TASDATA tas_runmax.nc

export CDO_PCTL_NBINS=302

$CDO ydrunpctl,10,$windowDays,rm=$readMethod,pm=$percentileMethod $TASDATA tas_runmin.nc tas_runmax.nc tn10thresh.nc

# Duration indices

$CDO etccdi_csdi,6,freq=year $TASDATA tn10thresh.nc etccdi_csdi.nc

# Wet days

$CDO etccdi_cwd $PRDATA etccdi_cwd.nc

# Percentile based indices

export CDO_PCTL_NBINS=302

$CDO etccdi_tn10p,5,$YEARMIN,$YEARMAX $TASDATA tas_runmin.nc tas_runmax.nc etccdi_tn10p.nc
