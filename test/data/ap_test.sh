CDO=cdo
LEVELS=99000,98400,92500,85000,50000,20000,10000,2.3,1
$CDO -v  ap2pl,${LEVELS} -selname,ps,ta,pfull ap_l90.nc rta.nc
$CDO -v  ap2pl,${LEVELS} -selname,ps,wa,phalf ap_l90.nc rwa.nc
$CDO -v  intlevel,level=${LEVELS},zvarname=pfull -selname,ps,ta,pfull ap_l90.nc rta2.nc
$CDO -v  intlevel,level=${LEVELS},zvarname=phalf -selname,ps,wa,phalf ap_l90.nc rwa2.nc
$CDO diffn,abslim=0.001 rta.nc rta2.nc
$CDO diffn,abslim=0.001 rwa.nc rwa2.nc
