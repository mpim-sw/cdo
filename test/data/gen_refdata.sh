#!/bin/sh
#
CDO=cdo
#
FORMAT="-f srv -b F32"
#
########################################################################
#
# remapknn
#
cdo -f srv -b 32 remapknn,grid=global_5,k=4,weighted=rbf,rbf_scale=2 -temp,global_2 temp_remapknn_rbf_global_2_to_global_5_ref
cdo -f srv -b 32 remapknn,grid=global_5,k=4,weighted=rbf,rbf_scale=2 -temp,global_5 temp_remapknn_rbf_global_5_to_global_5_ref
cdo -f srv -b 32 remapknn,grid=global_2,k=4,weighted=rbf,rbf_scale=2 -temp,global_5 temp_remapknn_rbf_global_5_to_global_2_ref
cdo -f srv -b 32 remapknn,grid=global_5,k=16,weighted=gauss,gauss_scale=2 -temp,global_2 temp_remapknn_gauss_global_2_to_global_5_ref
cdo -f srv -b 32 remapknn,grid=global_5,k=16,weighted=gauss,gauss_scale=2 -temp,global_5 temp_remapknn_gauss_global_5_to_global_5_ref
cdo -f srv -b 32 remapknn,grid=global_2,k=16,weighted=gauss,gauss_scale=2 -temp,global_5 temp_remapknn_gauss_global_5_to_global_2_ref
cdo -f srv -b 32 remapknn,grid=global_2,k=9,weighted=avg -temp,global_5 temp_remapknn_avg_global_5_to_global_2_ref
cdo -f srv -b 32 remapknn,grid=global_5,k=9,weighted=avg -temp,global_5 temp_remapknn_avg_global_5_to_global_5_ref
cdo -f srv -b 32 remapknn,grid=global_5,k=9,weighted=avg -temp,global_2 temp_remapknn_avg_global_2_to_global_5_ref
#
########################################################################
#
# Fldstat
#
STATS="min max sum avg mean std std1 var var1 range skew kurt median"
IFILE=t21_geosp_tsurf_sea.grb
for STAT in $STATS; do
  $CDO $FORMAT fld$STAT $IFILE fld${STAT}_m_ref
done
exit
IFILE=t21_geosp_tsurf.grb
for STAT in $STATS; do
  $CDO $FORMAT fld$STAT $IFILE fld${STAT}_ref
done
exit
########################################################################
#
# COND2
#
OPERATORS="ifthenelse"
#
IFILE1=infile1
$CDO $FORMAT gtc,0 -topo,global_10 $IFILE1
IFILE2=infile2
$CDO $FORMAT -temp,global_10 $IFILE2
CONST=-77
CFILE=cfile
$CDO $FORMAT -const,$CONST,$IFILE1 $CFILE
#
for OPERATOR in $OPERATORS; do
  $CDO $FORMAT $OPERATOR $IFILE1 $IFILE2 $CFILE cond_${OPERATOR}_ref
done
#
rm $IFILE1 $IFILE2 $CFILE
#
#exit
#
########################################################################
#
# COND
#
OPERATORS="ifthen ifnotthen"
#
IFILE=infile
$CDO $FORMAT gtc,0 -topo,global_10 $IFILE
CONST=-77
CFILE=cfile
$CDO $FORMAT -const,$CONST,$IFILE $CFILE
#
for OPERATOR in $OPERATORS; do
  $CDO $FORMAT $OPERATOR $IFILE $CFILE cond_${OPERATOR}_ref
done
#
rm $IFILE $CFILE
#
#exit
#
########################################################################
#
# CONDC
#
OPERATORS="ifthenc ifnotthenc"
#
IFILE=infile
$CDO $FORMAT gtc,0 -topo,global_10 $IFILE
CONST=-77
#
for OPERATOR in $OPERATORS; do
  $CDO $FORMAT $OPERATOR,$CONST $IFILE cond_${OPERATOR}_ref
done
#
rm $IFILE
#
exit
#
########################################################################
#
# TIMSTAT3
#
$CDO $FORMAT remapnn,global_10 -detrend EH5_AMIP_1_TSURF_MM_1991-1995.nc  ts_anom_mm_5years
cdo selyear,1991 ts_anom_mm_5years temp_anom_mm_1991.srv
cdo selyear,1993 ts_anom_mm_5years temp_anom_mm_1993.srv
$CDO $FORMAT meandiff2test,3,0.05 temp_anom_mm_1991.srv temp_anom_mm_1993.srv meandiff2test_ref
$CDO $FORMAT varquot2test,3,0.05 temp_anom_mm_1991.srv temp_anom_mm_1993.srv varquot2test_ref
rm temp_anom_mm_1991.srv temp_anom_mm_1993.srv
#
exit
#
########################################################################
#
# SPLIT
#
IFILE=pl_data.grb
OPERATORS="splitcode splitparam splitname splitlevel splitgrid splitzaxis splittabnum"
#
for OPERATOR in $OPERATORS; do
    $CDO $OPERATOR $IFILE ${OPERATOR}_
done
exit
#
########################################################################
#
# PACK
#
set -x
IFILE=tsurf_5steps_land.nc
OPERATOR=pack
for NBITS in i16 i8 u16 u8; do
    $CDO -f nc4 -b $NBITS $OPERATOR $IFILE ${OPERATOR}_${NBITS}_ref
    $CDO -f nc unpack ${OPERATOR}_${NBITS}_ref unpack_${NBITS}_ref
done
exit
#
########################################################################
#
# Timfill
#
IFILE=timfilldata
#$CDO $FORMAT -seq,1,30 ${IFILE}.srv
#$CDO -setrtomiss,1,15 ${IFILE}.srv ${IFILE}1.srv
#$CDO -setrtomiss,16,30 ${IFILE}.srv ${IFILE}2.srv
#$CDO -setrtomiss,11,20 ${IFILE}.srv ${IFILE}3.srv
#$CDO -setrtomiss,28,30 -setrtomiss,18,23 -setrtomiss,12,14 -setrtomiss,7,8 -setrtomiss,3,3 ${IFILE}.srv ${IFILE}4.srv
OPERATOR="timfillmiss"
METHODS="nearest linear forward backward"
METHODS="linear"
for METHOD in $METHODS; do
  for NUM in {1..4}; do
    $CDO $FORMAT ${OPERATOR},method=${METHOD} ${IFILE}${NUM}.srv ${OPERATOR}_${METHOD}_${NUM}_ref
  done
done
exit
########################################################################
#
# Vertfill
#
IFILE=vertfillmiss
#$CDO -setrtomiss,1,15 ${IFILE}.srv ${IFILE}_1.srv
#$CDO -setrtomiss,16,30 ${IFILE}.srv ${IFILE}_2.srv
#$CDO -setrtomiss,11,20 ${IFILE}.srv ${IFILE}_3.srv
#$CDO -setrtomiss,28,30 -setrtomiss,18,23 -setrtomiss,12,14 -setrtomiss,7,8 -setrtomiss,3,3 ${IFILE}.srv ${IFILE}_4.srv
#exit
OPERATOR="vertfillmiss"
METHODS="nearest linear forward backward"
for METHOD in $METHODS; do
  for NUM in {1..4}; do
    $CDO $FORMAT ${OPERATOR},method=${METHOD} ${IFILE}${NUM}.srv ${OPERATOR}_${METHOD}_${NUM}_ref
  done
done
exit
########################################################################
#
# data_mm_5years
#
#IFILE=$HOME/data/cdt/cera/EH5_AMIP_1_TSURF_MM_1991-1995.grb
#OFILE=ts_mm_5years
#$CDO $FORMAT remapnn,lon=55_lat=10 $IFILE $OFILE
#
IFILE=$HOME/data/cdt/cera/EH5_AMIP_1_TPREC_MM_1991-1995.nc
OFILE=prec_mm_5years
$CDO $FORMAT remapnn,lon=55_lat=10 $IFILE $OFILE
#
IFILE=prec_mm_5years
OFILE=prec_mm_5years_m
$CDO $FORMAT setrtomiss,-1,1.e-5 $IFILE $OFILE
#
$CDO $FORMAT merge ts_mm_5years prec_mm_5years tp_mm_5years
$CDO $FORMAT merge ts_mm_5years_m prec_mm_5years_m tp_mm_5years_m
#
exit
########################################################################
#
# Remapstat
#
OPERATORS="remapnn remapdis remapcon remaplaf"
#
SRCGRID=hp32_nest
TGTGRID=global_5
for OPERATOR in $OPERATORS; do
  $CDO $FORMAT ${OPERATOR},${TGTGRID} -temp,${SRCGRID} temp_${OPERATOR}_${SRCGRID}_to_${TGTGRID}_ref
done
#
SRCGRID=global_2
TGTGRID=hp16_nest
for OPERATOR in $OPERATORS; do
  $CDO $FORMAT ${OPERATOR},${TGTGRID} -temp,${SRCGRID} temp_${OPERATOR}_${SRCGRID}_to_${TGTGRID}_ref
done
#
SRCGRID=hp16_nest
TGTGRID=hp16_nest
for OPERATOR in $OPERATORS; do
  $CDO $FORMAT ${OPERATOR},${TGTGRID} -temp,${SRCGRID} temp_${OPERATOR}_${SRCGRID}_to_${TGTGRID}_ref
done
#
exit
########################################################################
#
# Percentile methods
#
FORMAT="-f ext -b F32"
IFILE=topo5.srv
cdo -f srv topo,global_5 $IFILE
PCTLS="0 1 20 25 33 50 66 75 80 99 100"
METHODS="nrank nist rtype8 linear lower higher nearest midpoint inverted_cdf averaged_inverted_cdf closest_observation interpolated_inverted_cdf hazen weibull median_unbiased normal_unbiased"
for METHOD in $METHODS; do
  echo "method: ${METHOD}"
  for PCTL in $PCTLS; do
     $CDO $FORMAT --percentile $METHOD fldpctl,$PCTL $IFILE percentile_${PCTL}_${METHOD}_ref
#     $CDO -s $FORMAT --percentile $METHOD outputf,%g -fldpctl,$PCTL $IFILE
  done
done
exit
# ----- NumPy
cat > methods.py <<EOF
import xarray as xr
import numpy as np
np.version.version
ds=xr.load_dataset("topo5.nc", engine="netcdf4");
a=ds['topo'].data
for m in ['linear', 'lower', 'higher', 'nearest', 'midpoint', 'inverted_cdf', 'averaged_inverted_cdf', 'closest_observation', 'interpolated_inverted_cdf', 'hazen', 'weibull', 'median_unbiased', 'normal_unbiased'] :
   print(m)
   for p in [0, 1, 20, 25, 33, 50, 66, 75, 80, 99, 100] : print(np.percentile(a, p, method=m))
EOF
python methods.py
########################################################################
#
# Fldpctl
#
IFILE=t21_geosp_tsurf.grb
PCTLS="1 20 25 33 50 66 75 80 99 100"
for PCTL in $PCTLS; do
  $CDO $FORMAT fldpctl,$PCTL $IFILE fldpctl${PCTL}_ref
done
exit
########################################################################
#
# Remapstat
#
STATS="min max range sum avg mean std std1 var var1 skew kurt median"
#
SRCGRID=global_2
TGTGRID=global_5
for STAT in $STATS; do
  $CDO $FORMAT remap${STAT},${TGTGRID} -temp,${SRCGRID} temp_remap${STAT}_${SRCGRID}_to_${TGTGRID}_ref
done
#
SRCGRID=global_5
TGTGRID=global_2
for STAT in $STATS; do
  $CDO $FORMAT remap${STAT},${TGTGRID} -temp,${SRCGRID} temp_remap${STAT}_${SRCGRID}_to_${TGTGRID}_ref
done
#
SRCGRID=global_5
TGTGRID=global_5
for STAT in $STATS; do
  $CDO $FORMAT remap${STAT},${TGTGRID} -temp,${SRCGRID} temp_remap${STAT}_${SRCGRID}_to_${TGTGRID}_ref
done
exit
########################################################################
#
# Remap rotated grid
#
GRID1=global.grid1
GRID2=global.grid2
GRID3=rotated.grid
#
cat > $GRID1 << EOF
gridtype  = lonlat
xsize     = 720
ysize     = 360
xfirst    = 0.25
xinc      = 0.5
yfirst    = -89.75
yinc      = 0.5
EOF
#
cat > $GRID2 << EOF
gridtype  = lonlat
xsize     = 720
ysize     = 360
xfirst    = 0
xinc      = 0.5
yfirst    = -89.75
yinc      = 0.5
EOF
#
cat > $GRID3 << EOF
gridtype  = projection
xsize     = 76
ysize     = 53
xinc      = 0.24
yinc      = 0.24
xfirst    = -18.0
yfirst    = -12.9
grid_mapping_name = rotated_latitude_longitude
grid_north_pole_longitude = -170.0
grid_north_pole_latitude  = 43.0
EOF
#
for NUM in 1 2; do
  SRCGRID=global.grid${NUM}
# remapdis depends on extrapolation for curvilinear grids
  RMODS="bil bic dis nn con laf"
  RMODS="bil bic nn con"
  for RMOD in $RMODS; do
    for GRIDTYPE in regular curvilinear; do
      GRIDNAME=""
      SETGRID=""
      if [ "$GRIDTYPE" = "curvilinear" ]; then SETGRID="-setgridtype,curvilinear"; GRIDNAME="_curvi"; fi
      OFILE=remap${RMOD}_reg${NUM}${GRIDNAME}_rotated
      REMAP_EXTRAPOLATE=off $CDO $FORMAT remap${RMOD},${GRID3} ${SETGRID} -topo,${SRCGRID} ${OFILE}_ref
    done
  done
done
#rm -f $GRID1 $GRID2 $GRID3
exit
########################################################################
#
IFILE=bathy4.grb
#
cat > region0 <<EOF
-180 -90
180 -90
180 90
-180 90
EOF
#
cat > region1 <<EOF
-31 -9
51 -9
51 41
-31 41
EOF
#
cat > region2 <<EOF
140 -9
220 -9
220 41
140 41
EOF
#
OPERATOR=maskregion
#
for GRIDTYPE in regular curvilinear unstructured; do
    TYPE=${GRIDTYPE:0:1}
    SETGRID=""
    if [ "$GRIDTYPE" != "regular" ]; then SETGRID="-setgridtype,$GRIDTYPE"; fi
    for INDEX in 0 1 2; do
       $CDO $FORMAT ${OPERATOR},region${INDEX} $SETGRID $IFILE ${OPERATOR}_${TYPE}${INDEX}_ref
    done
done
exit
#
LONLATBOX[0]="-180,180,-90,90"
LONLATBOX[1]="-31,51,-9,41"
LONLATBOX[2]="140,220,-9,41"
#
OPERATOR=masklonlatbox
#
for GRIDTYPE in regular curvilinear unstructured; do
    TYPE=${GRIDTYPE:0:1}
    SETGRID=""
    if [ "$GRIDTYPE" != "regular" ]; then SETGRID="-setgridtype,$GRIDTYPE"; fi
    for INDEX in 0 1 2; do
       $CDO $FORMAT ${OPERATOR},${LONLATBOX[${INDEX}]} $SETGRID $IFILE ${OPERATOR}_${TYPE}${INDEX}_ref
    done
done
exit
#
INDEXBOX[0]="1,90,1,45"
INDEXBOX[1]="38,58,21,33"
#
OPERATOR=maskindexbox
#
for GRIDTYPE in regular curvilinear; do
    TYPE=${GRIDTYPE:0:1}
    SETGRID=""
    if [ "$GRIDTYPE" != "regular" ]; then SETGRID="-setgridtype,$GRIDTYPE"; fi
    for INDEX in 0 1; do
       $CDO $FORMAT ${OPERATOR},${INDEXBOX[${INDEX}]} $SETGRID $IFILE ${OPERATOR}_${TYPE}${INDEX}_ref
    done
done
exit
#
########################################################################
#
# Yseasstat Ymonstat Ydaystat
#
STATS="min max sum avg mean std std1 var var1 range"
#
IFILE=ts_mm_5years
for STAT in $STATS; do
  $CDO $FORMAT yseas$STAT $IFILE yseas${STAT}_ref
done
#
for STAT in $STATS; do
  $CDO $FORMAT ymon$STAT $IFILE ymon${STAT}_ref
done
#
for STAT in $STATS; do
  $CDO $FORMAT yday$STAT $IFILE yday${STAT}_ref
done
#
IFILE=ts_mm_5years_m
for STAT in $STATS; do
  $CDO $FORMAT yseas$STAT $IFILE yseas${STAT}m_ref
done
#
for STAT in $STATS; do
  $CDO $FORMAT ymon$STAT $IFILE ymon${STAT}m_ref
done
#
for STAT in $STATS; do
  $CDO $FORMAT yday$STAT $IFILE yday${STAT}m_ref
done
exit
########################################################################
#
# Dayarith
#
IFILE1=ts_6h_1mon
IFILE2=ts_1d_1year
OPS="add sub mul div"
for OP in $OPS; do
  $CDO $FORMAT day$OP $IFILE1 $IFILE2 day${OP}_ref
done
exit
########################################################################
#
# Monarith
#
IFILE1=ts_1d_1year
IFILE2=ts_mm_1991
OPS="add sub mul div"
for OP in $OPS; do
  $CDO $FORMAT mon$OP $IFILE1 $IFILE2 mon${OP}_ref
done
exit
########################################################################
#
# Ydayarith
#
IFILE1=ts_1d_5years
IFILE2=ts_1d_1year
OPS="add sub mul div"
for OP in $OPS; do
  $CDO $FORMAT yday$OP -del29feb $IFILE1 $IFILE2 yday${OP}_ref
done
exit
########################################################################
#
# Ymonarith
#
IFILE1=ts_mm_5years
IFILE2=ts_mm_1year
OPS="add sub mul div"
for OP in $OPS; do
  $CDO $FORMAT ymon$OP $IFILE1 $IFILE2 ymon${OP}_ref
done
exit
########################################################################
#
# Math
OPERS="abs int nint sqr sqrt exp ln log10 sin cos tan asin acos atan reci not"
IFILE=math_data
for OPER in $OPERS; do
  $CDO $FORMAT $OPER $IFILE math_${OPER}_ref
done
exit
#
########################################################################
#
# Merstat
#
STATS="min max range sum avg mean std std1 var var1 skew kurt median"
IFILE=t21_geosp_tsurf.grb
for STAT in $STATS; do
  $CDO $FORMAT mer$STAT $IFILE mer${STAT}_ref
done
exit
########################################################################
#
# Zonstat
#
STATS="min max range sum avg mean std std1 var var1 skew kurt median"
IFILE=t21_geosp_tsurf.grb
for STAT in $STATS; do
  $CDO $FORMAT zon$STAT $IFILE zon${STAT}_ref
done
exit
########################################################################
#
# Ensstat
#
IFILE=ts_mm_5years
export CDO_FILE_SUFFIX=NULL
$CDO splityear $IFILE ts_year
IFILE="ts_year????"
STATS="min max range sum avg mean std std1 var var1 skew kurt median"
for STAT in $STATS; do
  $CDO $FORMAT -O ens${STAT} $IFILE ens${STAT}_ref
done
rm -f $IFILE
# missval
IFILE=ts_mm_5years_m
export CDO_FILE_SUFFIX=NULL
$CDO splityear $IFILE ts_year
IFILE="ts_year????"
STATS="min max range sum avg mean std std1 var var1 skew kurt median"
for STAT in $STATS; do
  $CDO $FORMAT -O ens${STAT} $IFILE ens${STAT}m_ref
done
rm -f $IFILE
exit
########################################################################
#
# Ydrunstat
#
STATS="min max sum avg mean std std1 var var1"
#
IFILE=ts_1d_5years
for STAT in $STATS; do
  $CDO $FORMAT ydrun$STAT,8 $IFILE ydrun${STAT}_ref
done
#
exit
########################################################################
#
# Fldstat2
#
IFILE=t21_geosp_tsurf.grb
IFILE1=var1
$CDO selcode,129 $IFILE $IFILE1
IFILE2=var2
$CDO selcode,169 $IFILE $IFILE2
#
OFILE=fldcor_ref
$CDO $FORMAT fldcor $IFILE1 $IFILE2 $OFILE
OFILE=fldcovar_ref
$CDO $FORMAT fldcovar $IFILE1 $IFILE2 $OFILE
#
rm $IFILE1 $IFILE2
#
exit
########################################################################
#
# Vertint
#
#cdo monmean ECHAM5_T42L19.grb ECHAM5_T42L19monavg.grb
#cdo after ECHAM5_T42L19monavg.grb afterdata.grb <<EOF
# TYPE=20 CODE=129,130,152,156,157
#EOF
#
IFILE=afterdata.grb
OFILE=hl_l19.grb
$CDO fldmean $IFILE $OFILE
IFILE=$OFILE
OFILE=ml2plx_ref
$CDO $FORMAT ml2plx,100000,92500,85000,50000,20000 $IFILE $OFILE
exit
########################################################################
#
# Math
#
STATS="abs fint fnint sqr sqrt exp ln log10 sin cos tan asin acos atan reci not"
#
IFILE=topo.srv
$CDO $FORMAT topo,global_30 $IFILE
for STAT in $STATS; do
  $CDO $FORMAT $STAT $IFILE math_${STAT}_ref
done
exit
########################################################################
#
# Timstat Yearstat Monstat Daystat Runstat
#
#IFILE=$HOME/data/cdt/cera/EH5_AMIP_1_TSURF_6H_1991-1995.grb
#OFILE=ts_6h_5years
#$CDO $FORMAT remapnn,lon=55_lat=10 $IFILE $OFILE
#
#IFILE=$OFILE
#OFILE=ts_1d_5years
#$CDO $FORMAT daymean $IFILE $OFILE
#$CDO selmonth,1 -selyear,1991 $IFILE ts_6h_1mon
#
#IFILE=$OFILE
#OFILE=ts_mm_5years
#$CDO $FORMAT -settime,12:00:00 -setday,15 -monmean $IFILE $OFILE
#$CDO selyear,1991 $IFILE ts_1d_1year
#
STATS="min max sum avg mean std std1 var var1 range"
#
IFILE=ts_mm_5years
for STAT in $STATS; do
  $CDO $FORMAT seas${STAT} $IFILE seas${STAT}_ref
done
#
IFILE=ts_mm_5years
for STAT in $STATS; do
  $CDO $FORMAT run${STAT},12 $IFILE run${STAT}_ref
done
#
IFILE=ts_mm_5years
for STAT in $STATS; do
  $CDO $FORMAT tim$STAT $IFILE tim${STAT}_ref
done
#
IFILE=ts_mm_5years
for STAT in $STATS; do
  $CDO $FORMAT year$STAT $IFILE year${STAT}_ref
done
#
IFILE=ts_1d_1year
for STAT in $STATS; do
  $CDO $FORMAT mon$STAT $IFILE mon${STAT}_ref
done
#
IFILE=ts_6h_1mon
for STAT in $STATS; do
  $CDO $FORMAT day$STAT $IFILE day${STAT}_ref
done
#
# missval
#
IFILE=ts_mm_5years_m
for STAT in $STATS; do
  $CDO $FORMAT seas${STAT} $IFILE seas${STAT}m_ref
done
#
IFILE=ts_mm_5years_m
for STAT in $STATS; do
  $CDO $FORMAT run${STAT},12 $IFILE run${STAT}m_ref
done
#
IFILE=ts_mm_5years_m
for STAT in $STATS; do
  $CDO $FORMAT tim$STAT $IFILE tim${STAT}m_ref
done
#
IFILE=ts_mm_5years_m
for STAT in $STATS; do
  $CDO $FORMAT year$STAT $IFILE year${STAT}m_ref
done
#
IFILE=ts_1d_1year_m
for STAT in $STATS; do
  $CDO $FORMAT mon$STAT $IFILE mon${STAT}m_ref
done
#
IFILE=ts_6h_1mon_m
for STAT in $STATS; do
  $CDO $FORMAT day$STAT $IFILE day${STAT}m_ref
done
exit
########################################################################
#
# Yearmonstat
#
IFILE1=ts_mm_5years
STATS="mean avg"
for STAT in $STATS; do
  $CDO $FORMAT yearmon$STAT $IFILE1  yearmon${STAT}_ref
done
exit
#
########################################################################
#
# Varsstat
#
STATS="min max sum avg mean std std1 var var1 range"
IFILE=vars_data.grb
for STAT in $STATS; do
  $CDO $FORMAT vars$STAT $IFILE vars${STAT}_ref
done
exit
########################################################################
#
# Remap regional grid
#
GRID=spain.grid
cat > $GRID <<EOF
gridtype=lonlat
xsize=20
ysize=18
xfirst=-13
yfirst=33
xinc=.8
yinc=.8
EOF
RMODS="bil bic dis nn con con2 scon laf"
IFILE=tsurf_spain.grb
for RMOD in $RMODS; do
  OFILE=tsurf_spain_${RMOD}
  for extra in def off on; do
      EXTRA="$extra"
      if [ "$EXTRA" = "def" ]; then EXTRA=""; fi
      REMAP_EXTRAPOLATE=$EXTRA $CDO $FORMAT remap${RMOD},${GRID} $IFILE ${OFILE}_${extra}_ref
  done
done
exit
########################################################################
#
# Filter
#
IFILE=ts_mm_5years
#
OFILE=lowpass_ref
$CDO $FORMAT lowpass,1 $IFILE $OFILE
OFILE=highpass_ref
$CDO $FORMAT highpass,1 $IFILE $OFILE
OFILE=bandpass_ref
$CDO $FORMAT bandpass,.5,5 $IFILE $OFILE
#
exit
########################################################################
#
# MapReduce
#
for grid  in r18x9 icon_cell; do
  REFGRID="${grid}_grid"
  $CDO -f nc -temp,$REFGRID data_${grid}.nc
  $CDO -f nc -gtc,273.15 -temp,$REFGRID mask_${grid}.nc
  $CDO -f nc reducegrid,mask_${grid}.nc data_${grid}.nc reduced_${grid}.nc
  $CDO griddes reduced_${grid}.nc > griddes.${grid}
  rm -f data_${grid}.nc mask_${grid}.nc reduced_${grid}.nc
done
exit
########################################################################
#
# Timstat2
#
IFILE=ts_mm_5years
IFILE1=ts_mm_2years1
$CDO selyear,1991,1992 $IFILE $IFILE1
IFILE2=ts_mm_2years2
$CDO selyear,1993,1994 $IFILE $IFILE2
#
OFILE=timcor_ref
$CDO $FORMAT timcor $IFILE1 $IFILE2 $OFILE
OFILE=timcovar_ref
$CDO $FORMAT timcovar $IFILE1 $IFILE2 $OFILE
#
rm $IFILE1 $IFILE2
#
exit
########################################################################
#
# Remap regional grid
#
RMODS="bil bic dis nn con con2 scon laf"
#cdo -f grb topo,europe_5 topo_eu5.grb
IFILE=topo_eu5.grb
$CDO -f grb -topo,europe_5 $IFILE
for RMOD in $RMODS; do
  OFILE=topo_eu5_${RMOD}
  for extra in def off on; do
      EXTRA="$extra"
      if [ "$EXTRA" = "def" ]; then EXTRA=""; fi
      REMAP_EXTRAPOLATE=$EXTRA $CDO $FORMAT remap${RMOD},global_5 $IFILE ${OFILE}_${extra}_ref
  done
done
exit
########################################################################
#
# smooth
#
IFILE=t21_geosp_tsurf_sea.grb
#
OFILE=smooth
$CDO $FORMAT smooth,radius=5deg $IFILE ${OFILE}1_ref
$CDO $FORMAT smooth,radius=5deg,maxpoints=3 $IFILE ${OFILE}2_ref
$CDO $FORMAT smooth,radius=5deg,nsmooth=9 $IFILE ${OFILE}3_ref
exit
########################################################################
#
# Timpctl Yearpctl Monstat Daypctl
#
PCTLS="50"
#
IFILE=ts_mm_5years
for PCTL in $PCTLS; do
  $CDO $FORMAT yseaspctl,${PCTL} $IFILE yseasmin_ref yseasmax_ref yseaspctl${PCTL}_ref
done
#
IFILE=ts_mm_5years
for PCTL in $PCTLS; do
  $CDO $FORMAT ymonpctl,${PCTL} $IFILE ymonmin_ref ymonmax_ref ymonpctl${PCTL}_ref
done
#
IFILE=ts_mm_5years
for PCTL in $PCTLS; do
  $CDO $FORMAT ydaypctl,${PCTL} $IFILE ydaymin_ref ydaymax_ref ydaypctl${PCTL}_ref
done
#
IFILE=ts_mm_5years
for PCTL in $PCTLS; do
  $CDO $FORMAT seaspctl,${PCTL} $IFILE seasmin_ref seasmax_ref seaspctl${PCTL}_ref
done
#
IFILE=ts_mm_5years
for PCTL in $PCTLS; do
  $CDO $FORMAT timpctl,$PCTL $IFILE timmin_ref timmax_ref timpctl${PCTL}_ref
done
#
IFILE=ts_mm_5years
for PCTL in $PCTLS; do
  $CDO $FORMAT timselpctl,$PCTL,12 $IFILE yearmin_ref yearmax_ref timsel12pctl${PCTL}_ref
done
for PCTL in $PCTLS; do
  $CDO $FORMAT timselpctl,$PCTL,60 $IFILE timmin_ref timmax_ref timsel60pctl${PCTL}_ref
done
#
IFILE=ts_mm_5years
for PCTL in $PCTLS; do
  $CDO $FORMAT yearpctl,$PCTL $IFILE yearmin_ref yearmax_ref yearpctl${PCTL}_ref
done
#
IFILE=ts_1d_1year
for PCTL in $PCTLS; do
  $CDO $FORMAT monpctl,$PCTL $IFILE monmin_ref monmax_ref monpctl${PCTL}_ref
done
#
IFILE=ts_6h_1mon
for PCTL in $PCTLS; do
  $CDO $FORMAT daypctl,$PCTL $IFILE daymin_ref daymax_ref daypctl${PCTL}_ref
done
exit
#
########################################################################
#
# runpctl
#
IFILE=ts_mm_5years
PCTLS="1 20 25 33 50 66 75 80 99 100"
for PCTL in $PCTLS; do
  $CDO $FORMAT runpctl,$PCTL,12 $IFILE runpctl${PCTL}_ref
done
exit
#
########################################################################
#
# setmiss
#
IFILE=t21_geosp_tsurf_sea.grb
#
$CDO $FORMAT setmisstoc,0 $IFILE setmisstoc_ref
$CDO $FORMAT setmisstonn $IFILE setmisstonn_ref
$CDO $FORMAT setmisstodis $IFILE setmisstodis_ref
exit
########################################################################
#
# Remap
#
cdo -f grb setrtomiss,0,10000  -gridboxmean,8,8 -topo bathy4.grb
#
GRIDS="n16 n32"
RMODS="bil bic dis nn con con2 laf"
RMODS="con2"
IFILE=bathy4.grb
for GRID in $GRIDS; do
  for RMOD in $RMODS; do
    OFILE=${GRID}_${RMOD}
    $CDO $FORMAT remap${RMOD},$GRID $IFILE ${OFILE}_ref
  done
done
exit
########################################################################
#
# Test GRIB files
#
IFILES="testfile01 testfile02 testfile03"
for FILE in $IFILES; do
  IFILE=grib_${FILE}.grb
  OFILE=grib_${FILE}_sinfo_ref
  $CDO sinfo $IFILE > $OFILE
  OFILE=grib_${FILE}_info_ref
  $CDO info $IFILE > $OFILE
done
#
########################################################################
#
# Test netCDF files
#
IFILES="testfile01 testfile02"
for FILE in $IFILES; do
  IFILE=netcdf_${FILE}.nc
  OFILE=netcdf_${FILE}_sinfon_ref
  $CDO sinfon $IFILE > $OFILE
  OFILE=netcdf_${FILE}_infon_ref
  $CDO infon $IFILE > $OFILE
done
exit
########################################################################
#
# Mergetime
#
$CDO $FORMAT settaxis,2001-01-01,,1year -const,1,r1x1 mergetime_y1
$CDO $FORMAT settaxis,2002-01-01,,1year -const,2,r1x1 mergetime_y2
$CDO $FORMAT mergetime mergetime_y1 mergetime_y2 mergetime_y12
env SKIP_SAME_TIME=1 cdo mergetime mergetime_y2 mergetime_y12  mergetime_ref
env SKIP_SAME_TIME=1 cdo mergetime mergetime_y12 mergetime_y2  mergetime_ref2
exit
########################################################################
#
# EOF
#
export CDO_FILE_SUFFIX=NULL
#export CDO_SVD_MODE=danielson_lanczos
export CDO_WEIGHT_MODE=off
#
IFILE=psl_DJF_anom.grb
cdo $FORMAT eof,1 $IFILE eval_ref eof_ref
cdo $FORMAT eofcoeff eof_ref $IFILE pcoeff
exit
########################################################################
#
# Vertstat
#
STATS="min max sum avg mean std std1 var var1 int"
IFILE=pl_data.grb
for STAT in $STATS; do
  $CDO $FORMAT vert$STAT $IFILE vert${STAT}_ref
done
exit
########################################################################
#
# Comparision
#
STATS="eqc nec lec ltc gec gtc"
#
IFILE=comptest.srv
cdo $FORMAT copy t21_geosp_tsurf.grb $IFILE
for STAT in $STATS; do
  $CDO $FORMAT $STAT,300 $IFILE comp_${STAT}_ref
done
exit
########################################################################
#
# Arith
#
IFILE=arith1.srv
MASK=arithmask.srv
OPS="add sub mul div"
for OP in $OPS; do
  $CDO $FORMAT $OP $IFILE $MASK arith${OP}_ref
done
exit
########################################################################
#
# Enspctl
#
IFILE=ts_mm_5years
export CDO_FILE_SUFFIX=NULL
$CDO splityear $IFILE ts_year
IFILE="ts_year????"
PCTLS="1 20 25 33 50 66 75 80 99 100"
for PCTL in $PCTLS; do
  $CDO $FORMAT enspctl,$PCTL $IFILE enspctl${PCTL}_ref
done
rm -f $IFILE
exit
########################################################################
#
# Test File
#
$CDO $FORMAT cdiwrite,1,global_10,3,3,3 file_F32_srv_ref
#
########################################################################
#
# Spectral
#
IFILE=ECHAM5_T21L19monavg.grb
OFILE=t21_geosp_tsurf.grb
$CDO selname,geosp,tsurf $IFILE $OFILE
#
IFILE=$OFILE
OFILE=gp2sp_ref
$CDO gp2sp $IFILE $OFILE
OFILE=gp2spl_ref
$CDO gp2spl $IFILE $OFILE
IFILE=gp2sp_ref
OFILE=sp2gp_ref
$CDO sp2gp $IFILE $OFILE
IFILE=gp2spl_ref
OFILE=sp2gpl_ref
$CDO sp2gpl $IFILE $OFILE
########################################################################
#
# Select
#
IFILE=ECHAM5_T21L19.grb
OFILE=pl_data.grb
$CDO -ml2pl,90000,9000,900,90 -remapbil,global_30 -sp2gp -seltimestep,1/5 -dayavg -selcode,129,130,152 $IFILE $OFILE
IFILE=$OFILE
OFILE=select1_ref
$CDO select,code=130,152 $IFILE $OFILE
OFILE=select2_ref
$CDO select,code=130,152,level=9000,90 $IFILE $OFILE
OFILE=select3_ref
$CDO select,code=130,152,level=9000,90,timestep=2,3,5 $IFILE $OFILE
OFILE=select4_ref
$CDO select,code=130 $IFILE $OFILE
OFILE=select5_ref
$CDO select,level=90000 $IFILE $OFILE
########################################################################
#
# Detrend
#
OFILE=detrend_data
$CDO $FORMAT -sqr -for,1,100 $OFILE
IFILE=$OFILE
OFILE=detrend_ref
$CDO detrend $IFILE $OFILE
########################################################################
