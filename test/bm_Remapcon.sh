#!/bin/sh
#
export DYLD_LIBRARY_PATH=/opt/intel/lib:$DYLD_LIBRARY_PATH
CDO=src/cdo
#
#
$CDO -V
#
for N in 3 4 5 6 7 8 9; do
  IFILE=topor2b$N
  $CDO -f nc4 topo,icor2b$N $IFILE
  OFILE=${IFILE}ycon
  echo "process $IFILE"
  time $CDO -P 1  remapcon,global_0.2 $IFILE ${OFILE}
done

#1 levante: gcc 11.2.0 avx2 (interactiv)  cdo-2.0.5
#       3   4   5    6    7     8      9     10    11    12
#01                                  122    394  1329  5351
#02                                   68    253   871
#04                                   46    159   563  2389
#08                                   31    128   446  1815
#16                                   24     86   339  1597
#32                                   20     79   322  1380
#64                                   17     69   314  1249

#1 levante: gcc 11.2.0 avx2 (interactiv)  cdo-2.0.4
#       3   4   5    6    7     8      9     10    11    12
#01                                  143    481  1788
#02                                   88    370  1315
#04                                   66    300  1084
#08                                   53    243   935
#16                                   44    200   861
#32                                   41    188   851  6494

#1 mistral: intel 18.0.5 avx2 (interactiv on mg207 node)  cdo-2.0.0rc6
#       3   4   5    6    7     8      9     10    11    12
#16     2   2   3    3    6    16     62    268  1207  6255
#36     2   2   2    3    5    14     55    250  1140  5962

#
# CDO version 1.9.5rc2
#
#1 bailung: gcc 7.2 sse4_2
#       3   4   5    6    7     8      9     10
#01    28  29  32   39   53    92    210
#08     5   5   6    7   10    21     65

#1 mistral: intel 18.0.1 avx2 (interactiv on mg208 node)
#       3   4   5    6    7     8      9     10    11    12
#01    13  15  17   21   32    67    156    527
#08     3   3   4    5    8    19     68    287
#16     2   2   3    3    6    16     62    272  1260  5831
#24     2   2   2    3    5    15     61    267

# CDO version 1.9.8
#
#1 hama: gcc 9.1 avx2
#       3   4   5    6    7     8      9     10
#01    16  17  19   23   32    58    136
#04     5   6   6    7   11    21     54

# CDO version 2.0.0 (YAC2)
#
#1 hama: gcc 10.2 avx2
#       3   4   5    6    7     8      9     10
#01    16  16  18   22   31    55    129
#04     5   5   6    7   10    19     50

# CDO version 2.1.0 (YAC2)
#
#1 hama: clang 15.1 avx2
#       3   4   5    6    7     8      9     10
#01     7   7   8   10   14    24     59
#04     2   2   3    3    5     8     21

# CDO 2.4.0 (YAC3.0.1)
#
#1 hama: clang 16.0.6 avx2
#       3   4   5    6    7     8      9     10
#01    10  11  12   15   23    43    125
#04     3   4   4    5    7    15     44

# CDO 2.4.0 (YAC3.1)
#
#1 hama: clang 16.0.6 avx2
#       3   4   5    6    7     8      9     10
#01     9  10  12   14   21    40    100
#04     3   3   4    5    7    13     38

# CDO 2.4.3 (YAC3.2)
#
#1 hama: clang 16.0.6 avx2
#       3   4   5    6    7     8      9     10
#01     9  10  11   14   20    38     99
#04     3   3   4    5    7    13     37

#1 mistral: intel 18.0.1 avx2 (interactiv on mg208 node)
# CDO version 1.9.8  -P 16
#           ncells        remapcon       remapnn
# R2B8 (   5242880)   [  22s   3GB]  [   3s   1GB]
# R2B9 (  20971520)   [  86s  10GB]  [  10s   3GB]
# R2B10(  83886080)   [ 376s  36GB]  [  34s  11GB]
# R2B11( 335544320)   [1664s 143GB]  [ 161s  43GB]
# R2B12(1342177280)   [7713s 566GB]  [ 711s 174GB]
# R2B13(5368709120)                  [3497s 699GB]
