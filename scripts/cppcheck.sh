#!/bin/bash
echo `cppcheck --version`

LOG_FILE=cppcheck_cdo.txt

SCRIPT_DIR=$(dirname "$0")
case $SCRIPT_DIR in
    "/"*)
        ;;
    ".")
        SCRIPT_DIR=$(pwd)
        ;;
    *)
        SCRIPT_DIR=$(pwd)/$(dirname "$0")
        ;;
esac

TOPDIR="$SCRIPT_DIR/.."
echo $TOPDIR

# --inconclusive  --template='{file}:{line},{severity},{id},{message}'
#         -I/opt/local/include/gcc14/c++ -I/opt/local/include/gcc14/c++/tr1 \
#  --check-level=exhaustive
# set -x
echo "" > ${LOG_FILE}
dirname=$TOPDIR/src
cppcheck --checkers-report=critical_errors --std=c++20  --force --enable=all --check-level=normal \
         --inline-suppr --template='{file}:{line},{severity},{id},{message}' \
         -i expr_lex.cc -i expr_yacc.cc -i NCL_wind.cc \
         -I${TOPDIR}/build/gnu/src -I${TOPDIR}/libcdi/src -I$TOPDIR/src -I$TOPDIR/src/mpim_grid -I/opt/local/include \
         -DHAVE_CONFIG_H  -DCDI_SIZE_TYPE=size_tL -DYAC_FOR_CDO -D__cpp_lib_ranges=201911L -D__cpp_lib_span=202002 \
         "$dirname" >>${LOG_FILE} 2>&1
#

#
echo "LOG_FILE: ${LOG_FILE}"
#
for PATTERN in warning error performance; do
    grep $PATTERN ${LOG_FILE}
done
echo "grep -v HAVE_CONFIG_H  ${LOG_FILE} | more"
