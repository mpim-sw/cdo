/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/
#ifndef PMLIST_H
#define PMLIST_H

#include <list>
#include <vector>
#include <string>

#include "listbuffer.h"
#include "namelist.h"
#include "cdo_cmor.h"

struct KeyValues
{
  int nvalues = 0;
  std::string key;
  std::vector<std::string> values;
};

// clang-format off
class  // KVList
#ifdef WARN_UNUSED
[[gnu::warn_unused]]
#endif
KVList : public std::list<KeyValues>
// clang-format on
{
public:
  std::string name;

  void print(FILE *fp = stderr) const;
  int parse_arguments(const std::vector<std::string> &argv);
  const KeyValues *search(std::string const &key) const;
  void remove(std::string const &inkey);
  void append(std::string const &key, const std::vector<std::string> &values, int nvalues);
  const std::string get_first_value(std::string const &key, std::string const &replacer);
};

// clang-format off
class  // PMList
#ifdef WARN_UNUSED
[[gnu::warn_unused]]
#endif
PMList : public std::list<KVList>
// clang-format on
{
public:
  const KVList *searchKVListVentry(std::string const &key, std::string const &value, const std::vector<std::string> &entry);
  const KVList *getKVListVentry(const std::vector<std::string> &entry);
  void print(FILE *fp = stderr);
  void read_namelist(FILE *fp, const char *name);
  void read_cmor_table(FILE *fp, const char *name);
};

void mapvar(int vlistID, int varID, const KeyValues &kv, CmorVar &cmorVar, bool &hasValidMin, bool &hasValidMax, int ptab,
            bool isnPtmodeName);
int parse_namelist(PMList &pmlist, NamelistParser &parser, char *buf, bool cdocmor);
int parse_list_buffer(NamelistParser &p, ListBuffer &listBuffer);

#endif
