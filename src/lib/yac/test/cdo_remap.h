#ifndef CDO_REMAP_H
#define CDO_REMAP_H

#include "cdo_grid.h"
#include "cdo_cell_search.h"

struct RemapSearch
{
  GridInfo srcGrid;
  GridInfo tgtGrid;
  GridCellSearch gcs;
};

enum class NormOpt
{
  NONE,
  DESTAREA,
  FRACAREA
};

void remap_conserv(NormOpt normOpt, const RemapSearch &remapSearch, const Varray<double> &srcArray, Varray<double> &tgtArray, double missval);
void remap_conserv2(NormOpt normOpt, const RemapSearch &remapSearch, const Varray<double> &srcArray, Varray<double> &tgtArray, double missval);

#endif
