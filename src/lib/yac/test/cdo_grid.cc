#include "cdo_grid.h"

static void
grid_gen_bounds(size_t n, const Varray<double> &centers, Varray<double> &bounds)
{
  const auto lrev = (centers[0] > centers[n - 1]);
  if (lrev)
    {
      for (size_t i = 0; i < n - 1; ++i)
        {
          bounds[2 * i] = 0.5 * (centers[i] + centers[i + 1]);
          bounds[2 * (i + 1) + 1] = 0.5 * (centers[i] + centers[i + 1]);
        }

      bounds[1] = 2 * centers[0] - centers[0];
      bounds[2 * n - 2] = 2 * centers[n - 1] - bounds[2 * n - 1];
    }
  else
    {
      for (size_t i = 0; i < n - 1; ++i)
        {
          bounds[2 * i + 1] = 0.5 * (centers[i] + centers[i + 1]);
          bounds[2 * (i + 1)] = 0.5 * (centers[i] + centers[i + 1]);
        }

      bounds[0] = 2 * centers[0] - bounds[1];
      bounds[2 * n - 1] = 2 * centers[n - 1] - bounds[2 * (n - 1)];
    }
}

static void
grid_gen_cellCornersLon2D(size_t nx, size_t ny, const Varray<double> &cellCornersLon, Varray<double> &cellCornersLon2D)
{
  for (size_t i = 0; i < nx; ++i)
    {
      const auto minLon = (cellCornersLon[0] > cellCornersLon[1]) ? cellCornersLon[2 * i + 1] : cellCornersLon[2 * i];
      const auto maxLon = (cellCornersLon[0] > cellCornersLon[1]) ? cellCornersLon[2 * i] : cellCornersLon[2 * i + 1];

      for (size_t j = 0; j < ny; ++j)
        {
          const auto index = 4 * (j * nx + i);
          cellCornersLon2D[index] = minLon;
          cellCornersLon2D[index + 1] = maxLon;
          cellCornersLon2D[index + 2] = maxLon;
          cellCornersLon2D[index + 3] = minLon;
        }
    }
}

static void
grid_gen_cellCornersLat2D(size_t nx, size_t ny, const Varray<double> &cellCornersLat, Varray<double> &cellCornersLat2D)
{
  for (size_t j = 0; j < ny; ++j)
    {
      const auto minLat = (cellCornersLat[0] > cellCornersLat[1]) ? cellCornersLat[2 * j + 1] : cellCornersLat[2 * j];
      const auto maxLat = (cellCornersLat[0] > cellCornersLat[1]) ? cellCornersLat[2 * j] : cellCornersLat[2 * j + 1];

      for (size_t i = 0; i < nx; ++i)
        {
          const auto index = 4 * (j * nx + i);
          cellCornersLat2D[index] = minLat;
          cellCornersLat2D[index + 1] = minLat;
          cellCornersLat2D[index + 2] = maxLat;
          cellCornersLat2D[index + 3] = maxLat;
        }
    }
}

void
generate_grid_lonlat(GridInfo &grid, double inc, double lon1, double lon2, double lat1, double lat2)
{
  // generate lon/lat grid with dx/dy of increment
  // the units of all computed coordinates are radians

  const size_t nlon = (size_t) ((lon2 - lon1) / inc + 0.5);
  const size_t nlat = (size_t) ((lat2 - lat1) / inc + 0.5);

  Varray<double> cellCentersLon(nlon);
  Varray<double> cellCentersLat(nlat);

  for (size_t i = 0; i < nlon; ++i) cellCentersLon[i] = lon1 + inc * 0.5 + i * inc;
  for (size_t i = 0; i < nlat; ++i) cellCentersLat[i] = lat1 + inc * 0.5 + i * inc;
  for (size_t i = 0; i < nlon; ++i) cellCentersLon[i] *= DEG2RAD;
  for (size_t i = 0; i < nlat; ++i) cellCentersLat[i] *= DEG2RAD;

  const size_t numCorners = 4;
  const size_t numCells = nlon * nlat;
  grid.numCells = numCells;
  grid.numDims[0] = nlon;
  grid.numDims[1] = nlat;

  grid.cellCentersLon.resize(numCells);
  grid.cellCentersLat.resize(numCells);

  for (size_t j = 0; j < nlat; ++j)
    for (size_t i = 0; i < nlon; ++i)
      {
        grid.cellCentersLon[j * nlon + i] = cellCentersLon[i];
        grid.cellCentersLat[j * nlon + i] = cellCentersLat[j];
      }

  grid.numCorners = numCorners;
  grid.cellCornersLon.resize(numCorners * numCells);
  grid.cellCornersLat.resize(numCorners * numCells);

  Varray<double> cellCornersLon(2 * nlon);
  Varray<double> cellCornersLat(2 * nlat);

  grid_gen_bounds(nlon, cellCentersLon, cellCornersLon);
  grid_gen_bounds(nlat, cellCentersLat, cellCornersLat);

  grid_gen_cellCornersLon2D(nlon, nlat, cellCornersLon, grid.cellCornersLon);
  grid_gen_cellCornersLat2D(nlon, nlat, cellCornersLat, grid.cellCornersLat);
}

GridInfo
generate_grid_reg2d_yac(double *coordinates_x, double *coordinates_y, size_t const (&num_cells)[2])
{
  GridInfo grid;
  // generate lon/lat grid from reg2d bounds
  // the units of all input and computed coordinates are radians

  const size_t nlon = num_cells[0];
  const size_t nlat = num_cells[1];

  Varray<double> cellCentersLon(nlon);
  Varray<double> cellCentersLat(nlat);

  for (size_t i = 0; i < nlon; ++i) cellCentersLon[i] = (coordinates_x[i] + coordinates_x[i + 1]) * 0.5;
  for (size_t i = 0; i < nlat; ++i) cellCentersLat[i] = (coordinates_y[i] + coordinates_y[i + 1]) * 0.5;

  const size_t numCorners = 4;
  const size_t numCells = nlon * nlat;
  grid.numCells = numCells;
  grid.numDims[0] = nlon;
  grid.numDims[1] = nlat;

  grid.cellCentersLon.resize(numCells);
  grid.cellCentersLat.resize(numCells);

  for (size_t j = 0; j < nlat; ++j)
    for (size_t i = 0; i < nlon; ++i)
      {
        grid.cellCentersLon[j * nlon + i] = cellCentersLon[i];
        grid.cellCentersLat[j * nlon + i] = cellCentersLat[j];
      }

  grid.numCorners = numCorners;
  grid.cellCornersLon.resize(numCorners * numCells);
  grid.cellCornersLat.resize(numCorners * numCells);

  Varray<double> cellCornersLon(2 * nlon);
  Varray<double> cellCornersLat(2 * nlat);

  for (size_t i = 0; i < nlon; ++i)
    {
      cellCornersLon[2 * i] = coordinates_x[i];
      cellCornersLon[2 * i + 1] = coordinates_x[i + 1];
    }
  for (size_t i = 0; i < nlat; ++i)
    {
      cellCornersLat[2 * i] = coordinates_y[i];
      cellCornersLat[2 * i + 1] = coordinates_y[i + 1];
    }

  grid_gen_cellCornersLon2D(nlon, nlat, cellCornersLon, grid.cellCornersLon);
  grid_gen_cellCornersLat2D(nlon, nlat, cellCornersLat, grid.cellCornersLat);

  return grid;
}

void
grid_print(GridInfo &grid)
{
  printf("gridtype  = curvilinear\n");
  printf("gridsize  = %zu\n", grid.numCells);
  printf("xsize     = %zu\n", grid.numDims[0]);
  printf("ysize     = %zu\n", grid.numDims[1]);
  printf("xvals     = ");
  for (size_t i = 0; i < grid.numCells; i++) printf("%g ", grid.cellCentersLon[i] * RAD2DEG);
  printf("\n");
  printf("yvals     = ");
  for (size_t i = 0; i < grid.numCells; i++) printf("%g ", grid.cellCentersLat[i] * RAD2DEG);
  printf("\n");
  printf("xbounds   = ");
  for (size_t i = 0; i < grid.numCells; i++)
    {
      for (size_t k = 0; k < grid.numCorners; k++) printf("%g ", grid.cellCornersLon[i * grid.numCorners + k] * RAD2DEG);
      printf("\n");
    }
  printf("ybounds   = ");
  for (size_t i = 0; i < grid.numCells; i++)
    {
      for (size_t k = 0; k < grid.numCorners; k++) printf("%g ", grid.cellCornersLat[i * grid.numCorners + k] * RAD2DEG);
      printf("\n");
    }
}
