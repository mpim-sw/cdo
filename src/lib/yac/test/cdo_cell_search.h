#ifndef CDO_CELL_SEARCH_H
#define CDO_CELL_SEARCH_H

#include <cstdio>
#include "cdo_grid.h"
extern "C"
{
#include "grid_cell.h"
}

struct GridCell
{
  double *coordinates_x = nullptr;
  double *coordinates_y = nullptr;
  struct yac_grid_cell yacGridCell;
};

struct GridCellSearch
{
  double (*xyzCoords)[3] = nullptr;
  void *yacBndCircles = nullptr;
  void *yacSearch = nullptr;
};

void gridcell_search_create(GridCellSearch &gcs, const GridInfo &gridInfo);
void gridcell_search_delete(GridCellSearch &gcs);
size_t do_gridcell_search(const GridCellSearch &gcs, bool isReg2dCell, GridCell gridCell, Varray<size_t> &srchAddr);

#endif
