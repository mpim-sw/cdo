#ifndef CDO_GRID_H
#define CDO_GRID_H

#include <cstdio>
#include <cmath>
#include <vector>

template <typename T>
using Varray = std::vector<T>;

template <typename T>
inline void
varray_free(Varray<T> &v)
{
  v.clear();
  v.shrink_to_fit();
}

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288  // pi
#endif

#ifndef RAD_CONVERT
#define RAD_CONVERT
constexpr double RAD2DEG = 180.0 / M_PI;  // conversion for rad to deg
constexpr double DEG2RAD = M_PI / 180.0;  // conversion for deg to rad
#endif

static inline void
LL_to_XYZ(double lon, double lat, double *xyz)
{
  const auto cos_lat = std::cos(lat);
  xyz[0] = cos_lat * std::cos(lon);
  xyz[1] = cos_lat * std::sin(lon);
  xyz[2] = std::sin(lat);
}

struct GridInfo
{
  size_t numDims[2] = { 0, 0 };
  size_t numCells = 0;
  size_t numCorners = 0;
  Varray<double> cellCentersLon;
  Varray<double> cellCentersLat;
  Varray<double> cellCornersLon;
  Varray<double> cellCornersLat;
};

void grid_print(GridInfo &grid);
void generate_grid_lonlat(GridInfo &grid, double inc, double lon1, double lon2, double lat1, double lat2);
GridInfo generate_grid_reg2d_yac(double *coordinates_x, double *coordinates_y, size_t const (&num_cells)[2]);

#endif
