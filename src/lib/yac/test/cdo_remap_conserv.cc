#include <algorithm>  // std::sort
#include <numeric>    // std::accumulate

#include "remap_method_conserv.h"
#include "cdo_remap.h"

const auto is_not_equal = [](auto a, auto b) noexcept { return (a < b || b < a); };
const auto is_equal = [](auto a, auto b) noexcept { return !(a < b || b < a); };

static void
set_cell_coordinates_yac(size_t cellIndex, size_t numCorners, const GridInfo &gridInfo, const yac_grid_cell &yacGridCell, double *x,
                         double *y)
{
  auto storeXY = (x && y);
  auto xyz = yacGridCell.coordinates_xyz;

  const auto cellCornersLon = &gridInfo.cellCornersLon[cellIndex * numCorners];
  const auto cellCornersLat = &gridInfo.cellCornersLat[cellIndex * numCorners];
  for (size_t i = 0; i < numCorners; ++i) LL_to_XYZ(cellCornersLon[i], cellCornersLat[i], xyz[i]);

  if (storeXY)
    {
      for (size_t k = 0; k < numCorners; ++k)
        {
          x[k] = cellCornersLon[k];
          y[k] = cellCornersLat[k];
        }
    }
}

static void
set_cell_coordinates(size_t cellIndex, size_t numCorners, const GridInfo &gridInfo, const GridCell &gridCell)
{
  set_cell_coordinates_yac(cellIndex, numCorners, gridInfo, gridCell.yacGridCell, gridCell.coordinates_x, gridCell.coordinates_y);
}

static void
set_coordinates_yac(size_t numCells, const Varray<size_t> &cellIndices, size_t numCorners, const GridInfo &gridInfo,
                    const Varray<yac_grid_cell> &yacGridCell)
{
  for (size_t i = 0; i < numCells; ++i)
    set_cell_coordinates_yac(cellIndices[i], numCorners, gridInfo, yacGridCell[i], nullptr, nullptr);
}

static void
set_coordinates_yac(const double (*xyzCoords)[3], size_t numCells, const Varray<size_t> &cellIndices, size_t numCorners,
                    const Varray<yac_grid_cell> &yacGridCell)
{
  for (size_t i = 0; i < numCells; ++i)
    {
      auto offset = cellIndices[i] * numCorners;
      auto xyz = yacGridCell[i].coordinates_xyz;
      for (size_t k = 0; k < numCorners; ++k)
        for (size_t l = 0; l < 3; ++l) xyz[k][l] = xyzCoords[offset + k][l];
    }
}

static void
gridcell_init_yac(GridCell &gridCell, size_t numCorners, enum yac_edge_type *edgeType)
{
  gridCell.yacGridCell.array_size = numCorners;
  gridCell.yacGridCell.num_corners = numCorners;
  gridCell.yacGridCell.edge_type = edgeType;
  gridCell.yacGridCell.coordinates_xyz = new double[numCorners][3];
  gridCell.coordinates_x = new double[numCorners];
  gridCell.coordinates_y = new double[numCorners];
}

static void
gridcell_free_yac(const GridCell &gridCell)
{
  delete[] gridCell.yacGridCell.coordinates_xyz;
  delete[] gridCell.coordinates_x;
  delete[] gridCell.coordinates_y;
}

static size_t
remove_invalid_areas(size_t numSearchCells, Varray<double> &areas, Varray<size_t> &searchIndices)
{
  size_t n = 0;
  for (size_t i = 0; i < numSearchCells; ++i)
    {
      if (areas[i] > 0.0)
        {
          areas[n] = areas[i];
          searchIndices[n] = searchIndices[i];
          n++;
        }
    }

  return n;
}

static size_t
remove_invalid_weights(size_t gridSize, size_t numWeights, Varray<double> &weights, Varray<size_t> &searchIndices)
{
  size_t n = 0;
  for (size_t i = 0; i < numWeights; ++i)
    {
      auto cellIndex = (weights[i] > 0.0) ? searchIndices[i] : gridSize;
      if (cellIndex != gridSize)
        {
          weights[n] = weights[i];
          searchIndices[n] = cellIndex;
          n++;
        }
    }

  return n;
}

static size_t
remove_unmask_weights(const Varray<int8_t> &gridMask, size_t numWeights, Varray<double> &weights, Varray<size_t> &indices)
{
  size_t n = 0;
  for (size_t i = 0; i < numWeights; ++i)
    {
      auto index = indices[i];
      /*
        Store the appropriate addresses and weights.
        Also add contributions to cell areas.
        The source grid mask is the master mask.
      */
      if (gridMask[index])
        {
          weights[n] = weights[i];
          indices[n] = index;
          n++;
        }
    }

  return n;
}

static void
normalize_weights(NormOpt normOpt, double cellArea, double cellFrac, size_t numWeights, Varray<double> &weights)
{
  if (normOpt == NormOpt::DESTAREA)
    {
      auto normFactor = is_not_equal(cellArea, 0.0) ? 1.0 / cellArea : 0.0;
      for (size_t i = 0; i < numWeights; ++i) weights[i] *= normFactor;
    }
  else if (normOpt == NormOpt::FRACAREA)
    {
      auto normFactor = is_not_equal(cellFrac, 0.0) ? 1.0 / cellFrac : 0.0;
      for (size_t i = 0; i < numWeights; ++i) weights[i] *= normFactor;
    }
  else if (normOpt == NormOpt::NONE) {}
}

static void
correct_weights(double cellArea, size_t numWeights, Varray<double> &weights)
{
  for (size_t i = 0; i < numWeights; ++i) weights[i] /= cellArea;
  yac_correct_weights(numWeights, weights.data());
  for (size_t i = 0; i < numWeights; ++i) weights[i] *= cellArea;
}

static void
sort_weights(size_t numWeights, Varray<size_t> &searchIndices, Varray<double> &weights)
{
  size_t n;
  for (n = 1; n < numWeights; ++n)
    if (searchIndices[n] < searchIndices[n - 1]) break;
  if (n == numWeights) return;

  if (numWeights > 1)
    {
      struct IndexWeightX
      {
        size_t index;
        double weight;
      };

      Varray<IndexWeightX> indexWeights(numWeights);

      for (n = 0; n < numWeights; ++n)
        {
          indexWeights[n].index = searchIndices[n];
          indexWeights[n].weight = weights[n];
        }

      const auto compareIndex = [](const auto &a, const auto &b) noexcept { return a.index < b.index; };
      std::sort(indexWeights.begin(), indexWeights.end(), compareIndex);

      for (n = 0; n < numWeights; ++n)
        {
          searchIndices[n] = indexWeights[n].index;
          weights[n] = indexWeights[n].weight;
        }
    }
}

static int
get_lonlat_circle_index(size_t numCells, size_t numCorners, const Varray<double> &clon, const Varray<double> &clat)
{
  int lonlatCircleIndex = -1;

  if (numCorners == 4)
    {
      size_t iadd = (numCells < 100) ? 1 : numCells / 30 - 1;
      size_t num_i = 0, num_eq0 = 0, num_eq1 = 0;

      for (size_t i = 0; i < numCells; i += iadd)
        {
          auto i4 = i * 4;
          num_i++;
          // clang-format off
          if (is_equal(clon[i4 + 1], clon[i4 + 2]) && is_equal(clon[i4 + 3], clon[i4 + 0]) &&
              is_equal(clat[i4 + 0], clat[i4 + 1]) && is_equal(clat[i4 + 2], clat[i4 + 3]))
            {
              num_eq1++;
            }
          else if (is_equal(clon[i4 + 0], clon[i4 + 1]) && is_equal(clon[i4 + 2], clon[i4 + 3]) &&
                   is_equal(clat[i4 + 1], clat[i4 + 2]) && is_equal(clat[i4 + 3], clat[i4 + 0]))
            {
              num_eq0++;
            }
          // clang-format on
        }

      if (num_i == num_eq1) lonlatCircleIndex = 1;
      if (num_i == num_eq0) lonlatCircleIndex = 0;
    }

  // printf("lonlatCircleIndex %d\n", lonlatCircleIndex);

  return lonlatCircleIndex;
}

static int
get_lonlat_circle_index(const GridInfo &gridInfo)
{
  return get_lonlat_circle_index(gridInfo.numCells, gridInfo.numCorners, gridInfo.cellCornersLon, gridInfo.cellCornersLat);
}

static double
conserv_remap(const Varray<double> &srcArray, size_t numWeights, const Varray<double> &weights, const Varray<size_t> &srcIndices)
{
  double tgtPoint = 0.0;
  for (size_t i = 0; i < numWeights; ++i) tgtPoint += srcArray[srcIndices[i]] * weights[i];

  return tgtPoint;
}

void
remap_conserv(NormOpt normOpt, const RemapSearch &remapSearch, const Varray<double> &srcArray, Varray<double> &tgtArray,
              double missval)
{
  auto &srcGrid = remapSearch.srcGrid;
  auto &tgtGrid = remapSearch.tgtGrid;

  auto srcNumCells = srcGrid.numCells;
  auto tgtNumCells = tgtGrid.numCells;

  Varray<int8_t> srcGridMask(srcNumCells);
  for (size_t i = 0; i < srcNumCells; ++i) srcGridMask[i] = !is_equal(srcArray[i], missval);

  auto srcNumCorners = srcGrid.numCorners;
  auto tgtNumCorners = tgtGrid.numCorners;

  enum yac_edge_type lonlatCircleType[]
      = { YAC_LON_CIRCLE_EDGE, YAC_LAT_CIRCLE_EDGE, YAC_LON_CIRCLE_EDGE, YAC_LAT_CIRCLE_EDGE, YAC_LON_CIRCLE_EDGE };
  Varray<enum yac_edge_type> greatCircleType(std::max(srcNumCorners, tgtNumCorners), YAC_GREAT_CIRCLE_EDGE);

  auto srcEdgeType = greatCircleType.data();
  auto tgtEdgeType = greatCircleType.data();

  enum yac_cell_type tgtCellType = YAC_MIXED_CELL;

  if (srcNumCorners == 4)
    {
      auto lonlatCircleIndex = get_lonlat_circle_index(srcGrid);
      if (lonlatCircleIndex >= 0) srcEdgeType = &lonlatCircleType[lonlatCircleIndex];
    }

  if (tgtNumCorners == 4)
    {
      auto lonlatCircleIndex = get_lonlat_circle_index(tgtGrid);
      if (lonlatCircleIndex >= 0)
        {
          tgtCellType = YAC_LON_LAT_CELL;
          tgtEdgeType = &lonlatCircleType[lonlatCircleIndex];
        }
    }

  GridCell tgtGridCell;
  gridcell_init_yac(tgtGridCell, tgtNumCorners, tgtEdgeType);

  CellSearch cellSearch;
  cellSearch.numCorners = srcNumCorners;
  cellSearch.edgeType = srcEdgeType;

  auto numCorners = srcNumCorners;  // num of corners of search cells

  Varray<size_t> searchIndices(srcNumCells);

  // Loop over target grid
  for (size_t tgtCellIndex = 0; tgtCellIndex < tgtNumCells; ++tgtCellIndex)
    {
      tgtArray[tgtCellIndex] = missval;

      // if (!tgtGrid.mask[tgtCellIndex]) continue;

      set_cell_coordinates(tgtCellIndex, tgtNumCorners, tgtGrid, tgtGridCell);

      // Get search cells
      auto numSearchCells = do_gridcell_search(remapSearch.gcs, false, tgtGridCell, searchIndices);

      if (numSearchCells == 0) continue;

      // Create search arrays

      cellSearch.realloc(numSearchCells);

      if (remapSearch.gcs.xyzCoords)
        set_coordinates_yac(remapSearch.gcs.xyzCoords, numSearchCells, searchIndices, numCorners, cellSearch.gridCells);
      else
        set_coordinates_yac(numSearchCells, searchIndices, numCorners, srcGrid, cellSearch.gridCells);

      if (tgtNumCorners < 4 || tgtCellType == YAC_LON_LAT_CELL)
        cdo_compute_overlap_info(numSearchCells, cellSearch, tgtGridCell);
      else
        cdo_compute_concave_overlap_info(numSearchCells, cellSearch, tgtGridCell);

      auto &partialWeights = cellSearch.partialAreas;

      auto numWeights = remove_invalid_areas(numSearchCells, partialWeights, searchIndices);

      auto tgtCellArea = gridcell_area(tgtGridCell.yacGridCell);

      if (normOpt == NormOpt::FRACAREA) correct_weights(tgtCellArea, numWeights, partialWeights);

      numWeights = remove_invalid_weights(srcNumCells, numWeights, partialWeights, searchIndices);
      numWeights = remove_unmask_weights(srcGridMask, numWeights, partialWeights, searchIndices);

      auto tgtCellFrac = std::accumulate(partialWeights.begin(), partialWeights.begin() + numWeights, 0.0);

      if (numWeights)
        {
          sort_weights(numWeights, searchIndices, partialWeights);
          // Normalize weights using target cell area if requested
          normalize_weights(normOpt, tgtCellArea, tgtCellFrac, numWeights, partialWeights);
          tgtArray[tgtCellIndex] = conserv_remap(srcArray, numWeights, partialWeights, searchIndices);
        }
    }

  // Finished with all cells: deallocate search arrays

  cellSearch.free();
  gridcell_free_yac(tgtGridCell);
}  // remap_conserv
