// #define WITH_XYZCOORDS 1

#include "cdo_cell_search.h"
#include "cdo_grid.h"

extern "C"
{
#include "sphere_part.h"
}

void
gridcell_search_create(GridCellSearch &gcs, const GridInfo &gridInfo)
{
  auto numCells = gridInfo.numCells;
  auto numCorners = gridInfo.numCorners;
  const auto &cellCornerLon = gridInfo.cellCornersLon;
  const auto &cellCornerLat = gridInfo.cellCornersLat;

  Varray<enum yac_edge_type> edgeTypes(numCorners, YAC_GREAT_CIRCLE_EDGE);
  yac_grid_cell yacCell;
  yacCell.coordinates_xyz = new double[numCorners][3];
  yacCell.edge_type = edgeTypes.data();
  yacCell.num_corners = numCorners;
  yacCell.array_size = numCorners;

  auto bndCircles = new bounding_circle[numCells];

#ifdef WITH_XYZCOORDS
  gcs.xyzCoords = new double[numCells * numCorners][3];
#endif

  for (size_t i = 0; i < numCells; ++i)
    {
      auto xyz = yacCell.coordinates_xyz;

      for (size_t k = 0; k < numCorners; ++k)
        LL_to_XYZ(cellCornerLon[i * numCorners + k], cellCornerLat[i * numCorners + k], xyz[k]);

      if (numCorners == 3)
        yac_get_cell_bounding_circle_unstruct_triangle(xyz[0], xyz[1], xyz[2], &bndCircles[i]);
      else
        yac_get_cell_bounding_circle(yacCell, &bndCircles[i]);

#ifdef WITH_XYZCOORDS
      auto offset = i * numCorners;
      for (size_t k = 0; k < numCorners; ++k)
        for (size_t l = 0; l < 3; ++l) gcs.xyzCoords[offset + k][l] = xyz[k][l];
#endif
    }

  gcs.yacSearch = yac_bnd_sphere_part_search_new(bndCircles, numCells);
  gcs.yacBndCircles = bndCircles;

  delete[] yacCell.coordinates_xyz;
}

void
gridcell_search_delete(GridCellSearch &gcs)
{
#ifdef WITH_XYZCOORDS
  if (gcs.xyzCoords) delete[] gcs.xyzCoords;
#endif
  if (gcs.yacSearch) yac_bnd_sphere_part_search_delete((bnd_sphere_part_search *) gcs.yacSearch);
  if (gcs.yacBndCircles) delete[] (bounding_circle *) gcs.yacBndCircles;

  gcs.xyzCoords = nullptr;
  gcs.yacSearch = nullptr;
  gcs.yacBndCircles = nullptr;
}

static size_t
do_gridcell_search_yac(bnd_sphere_part_search *yacSearch, bounding_circle *yacBndCircles, bool isReg2dCell,
                       yac_grid_cell yacGridCell, Varray<size_t> &srchAddr)
{
  size_t numCorners = yacGridCell.num_corners;
  bounding_circle bndCircle;
  auto xyz = yacGridCell.coordinates_xyz;

  if (numCorners == 4 && isReg2dCell)
    yac_get_cell_bounding_circle_reg_quad(xyz[0], xyz[1], xyz[2], &bndCircle);
  else if (numCorners == 3)
    yac_get_cell_bounding_circle_unstruct_triangle(xyz[0], xyz[1], xyz[2], &bndCircle);
  else
    yac_get_cell_bounding_circle(yacGridCell, &bndCircle);

  size_t numSearchCells;
  size_t *currNeighs;
  yac_bnd_sphere_part_search_do_bnd_circle_search(yacSearch, &bndCircle, 1, &currNeighs, &numSearchCells);

  if (srchAddr.size() < numSearchCells) srchAddr.resize(numSearchCells);

  size_t k = 0;
  // for (size_t i = 0; i < numSearchCells; ++i) srchAddr[i] = currNeighs[i];
  for (size_t i = 0; i < numSearchCells; ++i)
    {
      if (yac_extents_overlap(&bndCircle, &yacBndCircles[currNeighs[i]])) srchAddr[k++] = currNeighs[i];
    }
  numSearchCells = k;
  free(currNeighs);

  return numSearchCells;
}

size_t
do_gridcell_search(const GridCellSearch &gcs, bool isReg2dCell, GridCell gridCell, Varray<size_t> &srchAddr)
{
  return do_gridcell_search_yac((bnd_sphere_part_search *) gcs.yacSearch, (bounding_circle *) gcs.yacBndCircles, isReg2dCell,
                                gridCell.yacGridCell, srchAddr);
}
