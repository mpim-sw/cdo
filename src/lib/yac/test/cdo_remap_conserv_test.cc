/*
    gcc -DYAC_FOR_CDO -Wall -g -O3 -c ../*.c
    g++ -DYAC_FOR_CDO -Drestrict= -Wall -g -O3 -I.. cdo_remap_conserv_test.cc \
         cdo_grid.cc cdo_cell_search.cc cdo_remap_conserv.cc cdo_remap_conserv2.cc remap_method_conserv.cc *.o
 */

#include "cdo_remap.h"

static GridInfo
cdo_generate_grid_reg2d(std::vector<double> &coordinatesX, std::vector<double> &coordinatesY, size_t const (&numCells)[2])
{
  return generate_grid_reg2d_yac(coordinatesX.data(), coordinatesY.data(), numCells);
}

static GridInfo
generate_grid(std::vector<double> coordinatesX, std::vector<double> coordinatesY)
{
  size_t numCells[2] = { coordinatesX.size() - 1, coordinatesY.size() - 1 };
  for (size_t i = 0; i <= numCells[0]; ++i) coordinatesX[i] *= DEG2RAD;
  for (size_t i = 0; i <= numCells[1]; ++i) coordinatesY[i] *= DEG2RAD;

  return cdo_generate_grid_reg2d(coordinatesX, coordinatesY, numCells);
}

static void
test1()
{
  double missval = -1.0;
  RemapSearch remapSearch;

  auto &srcGrid = remapSearch.srcGrid;
  auto &tgtGrid = remapSearch.tgtGrid;

  srcGrid = generate_grid({ -1, 0, 1 }, { -1, 0, 1 });
  tgtGrid = generate_grid({ -0.5, 0.5 }, { -0.5, 0.5 });

  Varray<double> srcField(srcGrid.numCells);
  Varray<double> tgtField(tgtGrid.numCells);

  for (size_t i = 0; i < srcGrid.numCells; ++i) srcField[i] = i + 1;

  gridcell_search_create(remapSearch.gcs, srcGrid);

  remap_conserv(NormOpt::DESTAREA, remapSearch, srcField, tgtField, missval);

  gridcell_search_delete(remapSearch.gcs);

  double tgtFieldRef[] = { 2.5 };

  for (size_t i = 0; i < tgtGrid.numCells; ++i)
    {
      if (std::fabs(tgtFieldRef[i] - tgtField[i]) > 1e-3) printf("%s: wrong interpolation result\n", __func__);
    }
}

static void
test3()
{
  double missval = -1.0;
  RemapSearch remapSearch;

  auto &srcGrid = remapSearch.srcGrid;
  auto &tgtGrid = remapSearch.tgtGrid;

  srcGrid = generate_grid({ -1.5, -0.5, 0.5, 1.5 }, { -1.5, -0.5, 0.5, 1.5 });
  tgtGrid = generate_grid({ -1, 0, 1 }, { -1, 0, 1 });

  Varray<double> srcField(srcGrid.numCells);
  Varray<double> tgtField(tgtGrid.numCells);

  for (size_t i = 0; i < srcGrid.numCells; ++i) srcField[i] = i + 1;

  gridcell_search_create(remapSearch.gcs, srcGrid);

  remap_conserv(NormOpt::DESTAREA, remapSearch, srcField, tgtField, missval);

  gridcell_search_delete(remapSearch.gcs);

  double tgtFieldRef[] = { 3, 4, 6, 7 };

  for (size_t i = 0; i < tgtGrid.numCells; ++i)
    {
      if (std::fabs(tgtFieldRef[i] - tgtField[i]) > 1e-3) printf("%s: wrong interpolation result\n", __func__);
    }
}

static void
test4()
{
  double missval = -1.0;
  RemapSearch remapSearch;

  auto &srcGrid = remapSearch.srcGrid;
  auto &tgtGrid = remapSearch.tgtGrid;

  srcGrid = generate_grid({ 0, 1, 2, 3, 4 }, { 0, 1, 2, 3, 4 });
  tgtGrid = generate_grid({ 0.5, 1.5, 2.5, 3.5 }, { 0.5, 1.5, 2.5, 3.5 });

  Varray<double> srcField(srcGrid.numCells);
  Varray<double> tgtField(tgtGrid.numCells);

  //
  // mask for the source grid (cells with "x" are masked)
  //
  //       +---+---+---+---+
  //       | x | x | x |   |
  //       +---+---+---+---+
  //       | x | x |   |   |
  //       +---+---+---+---+
  //       | x |   |   |   |
  //       +---+---+---+---+
  //       |   |   |   |   |
  //       +---+---+---+---+
  int global_src_mask[] = { 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1 };
  for (size_t i = 0; i < srcGrid.numCells; ++i) srcField[i] = global_src_mask[i] ? i : missval;

  NormOpt normalisation[2] = { NormOpt::DESTAREA, NormOpt::FRACAREA };

  for (int norm_idx = 0; norm_idx <= 1; ++norm_idx)
    {
      gridcell_search_create(remapSearch.gcs, srcGrid);

      remap_conserv(normalisation[norm_idx], remapSearch, srcField, tgtField, missval);

      gridcell_search_delete(remapSearch.gcs);

      double tgtFieldRef[2][9]
          = { { 1.5, 3.5, 4.5, 1.25, 5.25, 8.5, -1, 2.5, 9.0 }, { 2.0, 3.5, 4.5, 5.0, 7.0, 8.5, -1, 10.0, 12.0 } };

      for (size_t i = 0; i < tgtGrid.numCells; ++i)
        {
          if (std::fabs(tgtFieldRef[norm_idx][i] - tgtField[i]) > 1e-3) printf("%s: wrong interpolation result\n", __func__);
        }
    }
}

static void
test6()
{
  double missval = -1.0;
  RemapSearch remapSearch;

  auto &srcGrid = remapSearch.srcGrid;
  auto &tgtGrid = remapSearch.tgtGrid;

  srcGrid = generate_grid({ -1.5, -0.5, 0.5, 1.5 }, { -1.5, -0.5, 0.5, 1.5 });
  tgtGrid = generate_grid({ -0.5, 0, 0.5 }, { -0.5, 0, 0.5 });

  // grid_print(srcGrid);
  // grid_print(tgtGrid);
  Varray<double> srcField(srcGrid.numCells);
  Varray<double> tgtField(tgtGrid.numCells);

  constexpr int NUM_TESTS = 4;

  double global_field_data[NUM_TESTS][9] = {
    { 1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 0, 0, 0, 1, 1, 1, 2, 2, 2 }, { 0, 1, 2, 0, 1, 2, 0, 1, 2 }, { 0, 1, 2, 1, 2, 3, 2, 3, 4 }
  };

  printf("test6\n");
  for (size_t t = 0; t < NUM_TESTS; ++t)
    {
      for (size_t i = 0; i < srcGrid.numCells; ++i) srcField[i] = global_field_data[t][i];

      gridcell_search_create(remapSearch.gcs, srcGrid);

      remap_conserv2(NormOpt::DESTAREA, remapSearch, srcField, tgtField, missval);

      gridcell_search_delete(remapSearch.gcs);

      double tgtFieldRef[NUM_TESTS][4]
          = { { 1, 1, 1, 1 }, { 0.75, 0.75, 1.25, 1.25 }, { 0.75, 1.25, 0.75, 1.25 }, { 1.5, 2.0, 2.0, 2.5 } };

      for (size_t i = 0; i < tgtGrid.numCells; ++i) printf("%zu %g %g\n", i + 1, tgtFieldRef[t][i], tgtField[i]);
      for (size_t i = 0; i < tgtGrid.numCells; ++i)
        {
          if (std::fabs(tgtFieldRef[t][i] - tgtField[i]) > 1e-3) printf("%s: wrong interpolation result\n", __func__);
        }
    }
}

int
main(void)
{
  // 1st order conservativ remapping
  test1();
  test3();
  test4();

  // 2nd order conservativ remapping
  test6();

  return 0;
}
