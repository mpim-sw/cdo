/*
    gcc -DYAC_FOR_CDO -Wall -g -O3 -c ../*.c
    g++ -DYAC_FOR_CDO -Drestrict= -Wall -g -O3 -I.. cdo_remap.cc cdo_grid.cc cdo_cell_search.cc cdo_remap_conserv.cc *.o
 */

#include "cdo_remap.h"

static void
compute_testfield(Varray<double> &array, const Varray<double> &cellCentersLon, const Varray<double> &cellCentersLat)
{
  auto numCells = array.size();
  for (size_t i = 0; i < numCells; ++i)
    {
      double xyz[3];
      LL_to_XYZ(cellCentersLon[i], cellCentersLat[i], xyz);
      auto x = xyz[0];
      auto y = xyz[1];
      auto z = xyz[2];
      array[i] = 1.0 + std::pow(x, 8.0) + std::exp(2.0 * y * y * y) + std::exp(2.0 * x * x) + 10.0 * x * y * z;
    }
}

static void
output_ext(const Varray<double> &array)
{
  auto numCells = array.size();
  fprintf(stdout, "%8d %4d %8g %8zu\n", 010101, 1, 0.0, numCells);

  int nout = 0;
  for (size_t i = 0; i < numCells; i++)
    {
      if (nout == 6)
        {
          nout = 0;
          fprintf(stdout, "\n");
        }
      fprintf(stdout, " %12.6g", array[i]);
      nout++;
    }
  fprintf(stdout, "\n");
}

static void
grid_init_regular(GridInfo &grid, double increment)
{
  generate_grid_lonlat(grid, increment, -180, 180, -90, 90);
}

int
main(void)
{
  double missval = -9.e33;
  RemapSearch remapSearch;

  auto &srcGrid = remapSearch.srcGrid;
  auto &tgtGrid = remapSearch.tgtGrid;

  grid_init_regular(srcGrid, 0.5);
  grid_init_regular(tgtGrid, 2.5);

  Varray<double> srcArray(srcGrid.numCells);
  Varray<double> tgtArray(tgtGrid.numCells);

  compute_testfield(srcArray, srcGrid.cellCentersLon, srcGrid.cellCentersLat);

  gridcell_search_create(remapSearch.gcs, srcGrid.numCells, srcGrid.numCorners, srcGrid.cellCornersLon, srcGrid.cellCornersLat);

  remap_conserv(NormOpt::FRACAREA, remapSearch, srcArray, tgtArray, missval);

  // output_ext(srcArray);
  output_ext(tgtArray);

  gridcell_search_delete(remapSearch.gcs);

  return 0;
}
