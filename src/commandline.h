#ifndef COMMANDLINE_H
#define COMMANDLINE_H

namespace cdo
{

void set_command_line(int argc, char **argv);
const char *command_line(void);

};  // namespace cdo

#endif
