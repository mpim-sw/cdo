// http://epaperpress.com/lexandyacc/download/LexAndYaccTutorial.pdf

// bison -W -y -r all -o expr_yacc.cc -d expr_yacc.y

%{
// clang-format off
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "expr.h"
#include "expr_yacc.hh" // expr_yacc.h (y.tab.h) is produced from expr_yacc.y by parser generator

// Bison manual p. 60 describes how to call yyparse() with arguments
// #define YYPARSE_PARAM parseArg
// #define YYLEX_PARAM   (ParseParamType &parseArg, void *yyscanner)

// #define YYPURE 1  ???

// prototypes
nodeType *expr_var(char *name);
nodeType *expr_con(double value);
nodeType *expr_cmd(const char *cname, char *vname);
nodeType *expr_fun(char *fname, int nops, ...);
nodeType *expr_opr(int oper, int nops, ...);

void free_node(nodeType *p);
%}

%define api.pure
%define parse.error verbose
%parse-param {ParseParamType &parseArg}
%parse-param {void *scanner}
// %lex-param {ParseParamType &parseArg}
%lex-param {yyscan_t *scanner}


%token <cvalue> CONSTANT
%token <varnm>  VARIABLE
%token <fname>  FUNCTION
%token QUESTION COLON
%token REMOVE
%token PRINT

%left AND OR
%left LEG GE LE EQ NE GT LT
%left '+' '-'
%left '*' '/'
%precedence UMINUS NOT
%right '^'

%type <nPtr> stmt expr stmt_list ternary

%%

program:
        function                  { return 0; }
        ;

function:
          function stmt           { expr_run($2, parseArg); free_node($2); }
        | %empty /* NULL */
        ;

stmt:
          ';'                      { $$ = expr_opr(';', 2, NULL, NULL); }
        | expr ';'                 { $$ = $1; }
        | VARIABLE '=' expr ';'    { $$ = expr_opr('=', 2, expr_var($1), $3); }
        | VARIABLE '=' ternary ';' { $$ = expr_opr('=', 2, expr_var($1), $3); }
        | VARIABLE ';'             { char *v = strdup($1); $$ = expr_opr('=', 2, expr_var($1), expr_var(v)); } /* conflicts: 1 shift/reduce */
        | REMOVE VARIABLE ')' ';'  { $$ = expr_cmd("remove", $2); }
        | PRINT VARIABLE ')' ';'   { $$ = expr_cmd("print", $2); }
        | '{' stmt_list '}'        { $$ = $2; }
        ;

stmt_list:
          stmt                    { $$ = $1; }
        | stmt_list stmt          { $$ = expr_opr(';', 2, $1, $2); }
        ;

expr:
          CONSTANT                { $$ = expr_con($1); }
        | VARIABLE                { $$ = expr_var($1); }
        | '-' expr %prec UMINUS   { $$ = expr_opr(UMINUS,  1, $2); }
        | NOT expr %prec NOT      { $$ = expr_opr(NOT,     1, $2); }
        | expr '+' expr           { $$ = expr_opr('+', 2, $1, $3); }
        | expr '-' expr           { $$ = expr_opr('-', 2, $1, $3); }
        | expr '*' expr           { $$ = expr_opr('*', 2, $1, $3); }
        | expr '/' expr           { $$ = expr_opr('/', 2, $1, $3); }
        | expr LT  expr           { $$ = expr_opr(LT,  2, $1, $3); }
        | expr GT  expr           { $$ = expr_opr(GT,  2, $1, $3); }
        | expr '^' expr           { $$ = expr_opr('^', 2, $1, $3); }
        | expr GE  expr           { $$ = expr_opr(GE,  2, $1, $3); }
        | expr LE  expr           { $$ = expr_opr(LE,  2, $1, $3); }
        | expr NE  expr           { $$ = expr_opr(NE,  2, $1, $3); }
        | expr EQ  expr           { $$ = expr_opr(EQ,  2, $1, $3); }
        | expr LEG expr           { $$ = expr_opr(LEG, 2, $1, $3); }
        | expr AND expr           { $$ = expr_opr(AND, 2, $1, $3); }
        | expr OR  expr           { $$ = expr_opr(OR,  2, $1, $3); }
        | '(' expr ')'            { $$ = $2; }
        | '(' ternary ')'         { $$ = $2; }
        | FUNCTION '(' expr ',' expr ',' expr ')'   { $$ = expr_fun($1, 3, $3, $5, $7); }
        | FUNCTION '(' expr ',' expr ')'            { $$ = expr_fun($1, 2, $3, $5); }
        | FUNCTION '(' expr ')'                     { $$ = expr_fun($1, 1, $3); }
        | FUNCTION '('  ')'                         { $$ = expr_fun($1, 1, NULL); }
        ;

ternary:  expr QUESTION expr COLON expr   { $$ = expr_opr('?', 3, $1, $3, $5); }
        ;

%%

nodeType *expr_con(double value)
{
  auto p = new nodeType;
  if (p == nullptr) yyerror(ParseParamType{}, nullptr, "Out of memory");

  p->type = NodeEnum::typeCon;
  p->v = conNodeType(value);
   
  return p;
}

nodeType *expr_var(char *name)
{
  auto p = new nodeType;
  if (p == nullptr) yyerror(ParseParamType{}, nullptr, "Out of memory");

  p->type = NodeEnum::typeVar;
  p->v = varNodeType(name);

  free(name);

  return p;
}

nodeType *expr_cmd(const char *cmdName, char *varName)
{
  auto p = new nodeType;
  if (p == nullptr) yyerror(ParseParamType{}, nullptr, "Out of memory");

  p->type = NodeEnum::typeCmd;
  p->v = cmdNodeType(cmdName, varName);

  return p;
}

nodeType *expr_fun(char *fname, int nops, ...)
{
  auto p = new nodeType;
  if (p == nullptr) yyerror(ParseParamType{}, nullptr, "Out of memory");

  p->type = NodeEnum::typeFun;

  va_list args;
  va_start(args, nops);
  p->v = funNodeType(fname, nops, args);
  va_end(args);

  if (fname) free(fname);

  return p;
}

nodeType *expr_opr(int oper, int nops, ...)
{
  auto p = new nodeType;
  if (p == nullptr) yyerror(ParseParamType{}, nullptr, "Out of memory");

  p->type = NodeEnum::typeOpr;

  va_list args;
  va_start(args, nops);
  p->v = oprNodeType(oper, nops, args);
  va_end(args);

  return p;
}

void free_node(nodeType *p)
{
  if (!p) return;

  if (p->type == NodeEnum::typeFun)
    {
      for (int i = 0; i < p->fun().nops; i++) free_node(p->fun().op[i]);
    }
  else if (p->type == NodeEnum::typeOpr)
    {
      for (int i = 0; i < p->opr().nops; i++) free_node(p->opr().op[i]);
    }
  
  delete p;
}

int CDO_parser_errorno = 0;

void yyerror(const ParseParamType &parseArg, void *scanner, const char *errstr)
{
  fprintf(stderr, "%s!\n", errstr);
  CDO_parser_errorno = -1;
}

#ifdef TEST_EXPR
int main(void)
{
  static char fexpr[] = "nvar = q*(geosp+234.56); xx = geosp+999-log(aps);";
  void *scanner;
  int yy_scan_string(const char *str, void *scanner);

  ParseParamType parseArg;

  printf("%s\n", fexpr);

  yylex_init(&scanner);
  yyset_extra(&parseArg, scanner);

  yy_scan_string(fexpr, scanner);

  parseArg.nvars1 = 0;
  parseArg.init = 1;
  parseArg.debug = 1;

  yyparse(parseArg, scanner);

  for (int i = 0; i < parseArg.nvars1; i++)
    printf("var %d %s\n", i, parseArg.params[i].name);

  yy_scan_string(fexpr, scanner);

  parseArg.init = 0;

  yyparse(parseArg, scanner);

  for (int i = 0; i < parseArg.nvars1; i++)
    printf("var %d %s\n", i, parseArg.params[i].name);

  return 0;
}
#endif

// clang-format on
