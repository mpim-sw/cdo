#ifndef POINT_H
#define POINT_H

class PointLonLat
{
public:
  PointLonLat() : m_lon(0.0), m_lat(0.0){};
  PointLonLat(double lon, double lat) : m_lon(lon), m_lat(lat){};

  // clang-format off
  double get_lon() const noexcept { return m_lon; };
  double get_lat() const noexcept { return m_lat; };
  // clang-format on

private:
  double m_lon;
  double m_lat;
};

#endif
